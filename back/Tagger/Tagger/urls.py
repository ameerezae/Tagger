from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Tagger API')

urlpatterns = [
    path('tagger/', schema_view),
    path('tagger/admin/', admin.site.urls),
    path('tagger/auth/', include('authentication.urls')),
    path('tagger/order/', include('order.urls')),
    path('tagger/search/', include('search.urls')),
    path('tagger/customer/', include('customer.urls')),
    path('tagger/tagger/', include('tagger.urls'))
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
