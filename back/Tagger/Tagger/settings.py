import os
from .configDB import debug_mode, db_config, host
import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '&f^tomg=cs^lu0a(i4u_20o+wo_mok^i$6ehz4zlm8!b@(o=5&'

DEBUG = debug_mode

ALLOWED_HOSTS = ['127.0.0.1', 'api.armankadeh.ir', "185.208.77.207"]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework_swagger',
    'rest_framework',
    'corsheaders',
    'rest_auth',
    'authentication.apps.AuthenticationConfig',
    'order.apps.OrderConfig',
    'search.apps.SearchConfig',
    'django_filters',
    'customer.apps.CustomerConfig',
    'tagger.apps.TaggerConfig',
    'message.apps.MessageConfig'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'Tagger.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Tagger.wsgi.application'


DATABASES = {
    'default': db_config
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'fa'

TIME_ZONE = 'Asia/Tehran'

USE_I18N = True

USE_L10N = True

USE_TZ = False


STATIC_URL = '/tagger/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'
APPEND_SLASH = False

CORS_ORIGIN_WHITELIST = [
    "http://localhost:3001",
    "http://127.0.0.1:3001",
    "http://localhost:3000",
    "http://127.0.0.1:3000",
    "https://tagger.armankadeh.ir"
]
CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.locale.LocaleMiddleware'
]
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
SSO_SECRET_KEY = 'j_y(y-4i8o!l(n@zvx+pfs(_$sgtg!r9eurmspvo$2y$#47$t8'
HOST = host
REST_FRAMEWORK = {'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'}

REST_USE_JWT = True
JWT_AUTH = {
    'JWT_AUTH_COOKIE': 'access-token',  # Name of Cookie
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),  # Expiration of jwt token
    # 'JWT_ALLOW_REFRESH': True,
    # 'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7)
}
ACCOUNT_AUTHENTICATION_METHOD = "username"

AUTH_USER_MODEL = "authentication.Customer"
