from rest_framework.serializers import ModelSerializer, Serializer, ValidationError
from rest_framework import serializers
from django.utils.translation import gettext
from authentication.models import Tagger
from .models import Order


class OrderSerializer(ModelSerializer):
    order_url = serializers.URLField(source='get_absolute_url', read_only=True)
    owner = serializers.CharField(source='customer.username', read_only=True)
    creation_date = serializers.DateField(source='creation_date.date', read_only=True)
    creation_datetime = serializers.DateField(source='creation_date.time', format="%H:%M:%S", read_only=True)
    duration = serializers.IntegerField(write_only=True, min_value=1,
                                        error_messages={"min_value": gettext("duration of order must be greater than one day.")})
    expiration_date = serializers.DateField(source='expiration_date.date', read_only=True)

    class Meta:
        model = Order
        fields = (
            'subject',
            'content',
            'tagger_number',
            'type',
            'creation_datetime',
            'creation_date',
            'order_url',
            'isPrivate',
            'price',
            'expiration_date',
            'duration',
            'slug',
            'owner',
            'is_edited'
        )
        read_only_fields = ('creation_date', 'slug', 'is_edited')

    def save(self, customer_id, **kwargs):
        data = self.validated_data
        order = Order(
            customer_id=customer_id,
            subject=data.get("subject"),
            content=data.get("content"),
            type=data.get("type"),
            tagger_number=data.get("tagger_number"),
            isPrivate=data.get("isPrivate"),
            price=data.get("price"),
            expiration_date=Order.exp_date_calculator(data.get("duration")),
            is_edited=False
        )

        order.save()


class TaggerSerializer(Serializer):
    username = serializers.CharField(required=True)

    def validate_username(self, value):
        tagger = Tagger.objects.filter(username=value).first()
        if not tagger:
            raise ValidationError(detail=gettext("user {} not found.").format(value))
        return tagger.id


class PrivateOrderSerializer(ModelSerializer):
    assigned = TaggerSerializer(many=True)
    duration = serializers.IntegerField(write_only=True, min_value=1,
                                        error_messages={"min_value": gettext("duration of order must be greater than one day.")})

    class Meta:
        model = Order
        fields = (
            'subject',
            'content',
            'type',
            'price',
            'duration',
            'assigned',
        )