from django.db import models
from django_jalali.db import models as jmodels
from django.utils.text import slugify
from Tagger.settings import HOST
from .constants import order_type, order_status, cooperation_status
from authentication.models import Customer, Tagger
import uuid, jdatetime


class Order(models.Model):
    guid = models.CharField(unique=True, max_length=40, blank=True, default="")
    slug = models.SlugField(max_length=255)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name="order")
    content = models.TextField()
    subject = models.CharField(max_length=100)
    type = models.CharField(choices=order_type.choices(), max_length=100)
    tagger_number = models.IntegerField(blank=True, null=True)
    creation_date = jmodels.jDateTimeField(auto_now_add=True)
    isPrivate = models.BooleanField(default=False)
    price = models.FloatField(null=True, blank=True)
    expiration_date = jmodels.jDateTimeField(blank=True, null=True)
    status = models.CharField(choices=order_status.choices(), max_length=100)
    is_edited = models.BooleanField(null=False, blank=False)

    def __str__(self):
        return str(self.subject)

    def save(self, *args, **kwargs):
        if not self.guid:
            self.guid = uuid.uuid4()
        self.slug = slugify(self.subject, allow_unicode=True)
        super(Order, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "%s/order/%s/%s/" % (HOST, self.customer.username, self.slug)

    @staticmethod
    def exp_date_calculator(duration):
        return jdatetime.datetime.now() + jdatetime.timedelta(duration)


class order_tagger(models.Model):
    tagger = models.ForeignKey(Tagger, on_delete=models.CASCADE, related_name="tagger")
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order")
    cooperation_status = models.CharField(choices=cooperation_status.choices(), max_length=100, default="requested")

    def __str__(self):
        return "%s__%s" % (self.order.slug, self.tagger.username)