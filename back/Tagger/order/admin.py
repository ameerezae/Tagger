from django.contrib import admin
from .models import Order, order_tagger

admin.site.register(Order)
admin.site.register(order_tagger)