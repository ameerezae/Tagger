from django.urls import re_path, path
from .views import OrderListView, OrderDetailView, SubmitOrderView, SubmitPrivateOrderView

urlpatterns = [
    re_path(r'^orders/$', OrderListView.as_view(), name="order_list"),
    path('submit/', SubmitOrderView.as_view(), name="order_submit"),
    path('submit/private/', SubmitPrivateOrderView.as_view(), name="private_order_submit"),
    path('<str:username>/<slug>/', OrderDetailView.as_view(), name="order_detail"),
]