import enum


class order_type(enum.Enum):
    textual = "متنی"

    @classmethod
    def choices(cls):
        return tuple((item.name, item.value) for item in cls)

    @classmethod
    def Dict(cls):
        return dict((item.name, item.value) for item in cls)


class order_status(enum.Enum):
    open = "open"
    closed = "closed"

    @classmethod
    def choices(cls):
        return tuple((item.name, item.value) for item in cls)

    @classmethod
    def Dict(cls):
        return dict((item.name, item.value) for item in cls)


class cooperation_status(enum.Enum):
    assigned = "assigned"
    requested = "requested"
    todo = "todo"
    done = "done"

    @classmethod
    def choices(cls):
        return tuple((item.name, item.value) for item in cls)

    @classmethod
    def Dict(cls):
        return dict((item.name, item.value) for item in cls)
