from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_201_CREATED
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.utils.translation import gettext
from .serializers import OrderSerializer, PrivateOrderSerializer
from .paginations import OrderPageNumberPagination
from .models import Order, order_tagger
from .constants import cooperation_status


class OrderListView(ListAPIView):
    """
    list of orders, use "page" query parameters for set page number.
    e.g: /?page=10
    """
    queryset = Order.objects.filter(isPrivate=False).all().order_by('-creation_date')
    allowed_methods = ('GET', )
    pagination_class = OrderPageNumberPagination
    serializer_class = OrderSerializer


class OrderDetailView(GenericAPIView):
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
    serializer_class = OrderSerializer
    allowed_method = ('GET',)

    def get(self, request, username, slug):
        """ get detail of an order that find with subject and creator username.
        404: not found.
        200: data.
        """
        order = Order.objects.filter(slug=slug, customer__username=username, isPrivate=False).first()
        if not order:
            return Response(data={"message": gettext("not found")}, status=HTTP_404_NOT_FOUND)
        serializer = OrderSerializer(instance=order)
        return Response(data=serializer.data, status=HTTP_200_OK)


class SubmitOrderView(GenericAPIView):
    allowed_methods = ('POST', )
    serializer_class = OrderSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        """
        submit an order.
        201: successfully created.
        400: payload errors.
        403: order exists.
        401: unauthorized.
        token required.
        type must be: "textual".
        """
        serializer = OrderSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

        if Order.objects.filter(subject=serializer.validated_data.get("subject"), customer_id=request.user.id).exists():
            return Response(data={"message": gettext("order already exists.")}, status=HTTP_403_FORBIDDEN)

        serializer.save(customer_id=request.user.id)
        return Response(data={"message": gettext("successfully submitted.")}, status=HTTP_201_CREATED)


class SubmitPrivateOrderView(GenericAPIView):
    allowed_methods = ('POST', )
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PrivateOrderSerializer

    def post(self, request):
        """
        submit an private order.
        201: successfully created.
        400: payload errors.
        403: order exists.
        401: unauthorized.
        token required.
        type must be: "textual"
        assigned field e.g: "assigned": [{"username": "alireza"},{"username": "mamad"}].
        """
        serializer = PrivateOrderSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

        if Order.objects.filter(subject=serializer.validated_data.get("subject"), customer_id=request.user.id).exists():
            return Response(data={"message": gettext("order already exists.")}, status=HTTP_403_FORBIDDEN)

        order = Order(
            customer_id=request.user.id,
            subject=serializer.validated_data.get("subject"),
            content=serializer.validated_data.get("content"),
            type=serializer.validated_data.get("type"),
            isPrivate=True,
            price=serializer.validated_data.get("price"),
            expiration_date=Order.exp_date_calculator(serializer.validated_data.get("duration")),
            is_edited=False
        )
        order.save()

        assigned = serializer.validated_data.get("assigned")
        for tagger in assigned:
            if order_tagger.objects.filter(tagger_id=tagger.get("username"), order=order).exists():
                continue
            assign_obj = order_tagger(
                tagger_id=tagger.get("username"),
                order=order,
                cooperation_status=cooperation_status.assigned.value
            )
            assign_obj.save()
        return Response(data={"message": gettext("successfully submitted.")}, status=HTTP_201_CREATED)
