from django.urls import path
from .views import TaggedOrdersView, RequestView, InboxView, TodoOrdersView

urlpatterns = [
    path('orders/tagged/', TaggedOrdersView.as_view(), name="tagged_orders"),
    path('orders/todo/', TodoOrdersView.as_view(), name="todo_orders"),
    path('request/<str:username>/<slug>/', RequestView.as_view(), name="request"),
    path('inbox/', InboxView.as_view(), name="tagger_inbox")
]