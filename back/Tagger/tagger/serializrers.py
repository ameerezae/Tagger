from rest_framework import serializers


class RequestSerializer(serializers.Serializer):
    message = serializers.CharField()