from django.db.models import Subquery
from django.utils.translation import gettext
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN
from django.contrib.contenttypes.models import ContentType
from .authentication import TaggerAuthentication
from order.serializers import OrderSerializer
from order.paginations import OrderPageNumberPagination
from order.models import Order, order_tagger
from order.constants import cooperation_status
from message.models import Message
from message.serializers import MessageSerializer
from .constants import RequestMessage


class TaggedOrdersView(ListAPIView):
    """
    get tagged orders.
    200: payload data.
    401: unauthorized.
    token required.
    """
    authentication_classes = [TaggerAuthentication]
    serializer_class = OrderSerializer
    allowed_methods = ('GET', )
    pagination_class = OrderPageNumberPagination

    def get_queryset(self):
        tagger = self.request.user
        orders_tagger = order_tagger.objects.filter(tagger_id=tagger.id, cooperation_status=cooperation_status.done.value).all()
        return Order.objects.filter(id__in=Subquery(orders_tagger.values('order_id'))).all().order_by('-creation_date')


class RequestView(GenericAPIView):
    authentication_classes = [TaggerAuthentication]
    allowed_methods = ('GET', )

    def get(self, request, username, slug):
        """
        send cooperation request for an order.
        404: order not found.
        403: request failed.
        200: request sent successfully.
        401: unauthorized.
        token required.
        """
        order = Order.objects.filter(slug=slug, customer__username=username, isPrivate=False).first()
        if not order:
            return Response(data={"message": gettext("not found")}, status=HTTP_404_NOT_FOUND)
        order_tagger_obj = order_tagger.objects.filter(tagger_id=request.user.id, order_id=order.id).first()
        if order_tagger_obj is not None:
            # return Response(data={"message": gettext("request failed.")}, status=HTTP_403_FORBIDDEN)
            return Response(data={"message": gettext("request failed.")}, status=HTTP_400_BAD_REQUEST)

        order_tagger_obj = order_tagger(
            order_id=order.id,
            tagger_id=request.user.id,
            cooperation_status=cooperation_status.requested.value
        )
        order_tagger_obj.save()
        request_message = Message(sender_obj=request.user,
                                  receiver_obj=order.customer,
                                  order_id=order.id,
                                  subject=RequestMessage.subject.value,
                                  content=RequestMessage.content.value.format(request.user.username, order.subject))
        request_message.save()
        return Response(data={"message": gettext("request sent successfully.")}, status=HTTP_200_OK)


class InboxView(GenericAPIView):
    authentication_classes = [TaggerAuthentication]
    allowed_methods = ('GET', )
    serializer_class = MessageSerializer

    def get(self, request):
        """
        get inbox messages.
        200: payload data.
        401: unauthorized.
        token required.
        """
        tagger_type = ContentType.objects.filter(model="tagger").first()
        messages = Message.objects.filter(receiver_id=request.user.id, receiver_type=tagger_type).all()
        serializer = MessageSerializer(instance=messages, many=True)
        return Response(data=serializer.data, status=HTTP_200_OK)


class TodoOrdersView(ListAPIView):
    """
    get todo orders.
    200: payload data.
    401: unauthorized.
    token required.
    """
    authentication_classes = [TaggerAuthentication]
    serializer_class = OrderSerializer
    allowed_methods = ('GET', )
    pagination_class = OrderPageNumberPagination

    def get_queryset(self):
        tagger = self.request.user
        orders_tagger = order_tagger.objects.filter(tagger_id=tagger.id, cooperation_status=cooperation_status.todo.value).all()
        return Order.objects.filter(id__in=Subquery(orders_tagger.values('order_id'))).all().order_by('-creation_date')
