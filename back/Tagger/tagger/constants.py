import enum
from django.utils.translation import gettext


class RequestMessage(enum.Enum):
    subject = gettext("Cooperation Request")
    content = gettext("User '{}' has sent a cooperation request for the '{}' order.")
