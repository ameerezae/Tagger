from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.utils.translation import ugettext as _
from authentication.models import Tagger
import jwt


class TaggerAuthentication(JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):
        username = payload.get("username")
        if not username:
            msg = _('Invalid payload.')
            raise AuthenticationFailed(msg)
        tagger = Tagger.objects.filter(username=username).first()
        if not tagger:
            msg = _('Invalid signature.')
            raise AuthenticationFailed(msg)
        return tagger

    def jwt_decode_handler(self, token):
        options = {
            'verify_exp': True,
        }
        secret_key = "j_y(y-4i8o!l(n@zvx+pfs(_$sgtg!r9eurmspvo$2y$#47$t8"
        return jwt.decode(
            token,
            secret_key,
            True,
            options=options,
            leeway=0,
            audience=None,
            issuer=None,
            algorithms=['HS256']
        )

    def authenticate(self, request):
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            msg = _("Authentication credentials were not provided.")
            raise AuthenticationFailed(msg)

        try:
            payload = self.jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = _('Signature has expired.')
            raise AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _('Error decoding signature.')
            raise AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise AuthenticationFailed()
        user = self.authenticate_credentials(payload)
        return (user, jwt_value)
