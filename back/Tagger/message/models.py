from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from order.models import Order


class Message(models.Model):
    sender_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="sender")
    sender_id = models.PositiveIntegerField()
    sender_obj = GenericForeignKey('sender_type', 'sender_id')
    receiver_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="receiver")
    receiver_id = models.PositiveIntegerField()
    receiver_obj = GenericForeignKey('receiver_type', 'receiver_id')
    subject = models.CharField(max_length=100)
    content = models.TextField(default="")
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_message")

    def __str__(self):
        return str(self.subject)