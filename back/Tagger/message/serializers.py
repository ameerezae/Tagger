from rest_framework import serializers
from .models import Message


class MessageSerializer(serializers.ModelSerializer):
    receiver = serializers.CharField(source='receiver_obj.username', read_only=True)
    sender = serializers.CharField(source='sender_obj.username', read_only=True)
    order = serializers.CharField(source='order.subject', read_only=True)
    order_slug = serializers.CharField(source='order.slug', read_only=True)
    order_url = serializers.URLField(source='order.get_absolute_url', read_only=True)

    class Meta:
        model = Message
        fields = (
            'sender',
            'receiver',
            'subject',
            'content',
            'order',
            'order_url',
            'order_slug',
        )