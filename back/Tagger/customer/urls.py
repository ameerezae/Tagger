from django.urls import path
from .views import OrdersView, OrderEditView, ProfileView, InboxView, AcceptRequestView, DeclineRequestView

urlpatterns = [
    path('orders/', OrdersView.as_view(), name="orders"),
    path('orders/<slug>/', OrderEditView.as_view(), name="order_edit"),
    path('profile/', ProfileView.as_view(), name="profile"),
    path('inbox/', InboxView.as_view(), name="customer_inbox"),
    path('accept/request/<str:username>/<slug>/', AcceptRequestView.as_view(), name="accept_request"),
    path('decline/request/<str:username>/<slug>/', DeclineRequestView.as_view(), name="decline_request")
]