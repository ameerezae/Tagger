from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from authentication.models import Customer
from Tagger.settings import HOST


class CustomeImageField(serializers.ImageField):
    def to_representation(self, value):
        if not value:
            return None

        use_url = getattr(self, 'use_url')
        if use_url:
            try:
                url = value.url
            except AttributeError:
                return None
            request = self.context.get('request', None)
            if request is not None:
                return request.build_absolute_uri(url)
            return HOST + url

        return value.name


class CustomerSerializer(ModelSerializer):
    image = CustomeImageField(required=False, use_url=True, allow_empty_file=True, allow_null=True)

    class Meta:
        model = Customer
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'image',
            'phone_number',
            'address',
        )
        read_only_fields = ('email', 'username')