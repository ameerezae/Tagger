from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils.translation import gettext
from django.contrib.contenttypes.models import ContentType
from .serializers import CustomerSerializer
from .constants import AcceptRequestMessage, DeclineRequestMessage
from order.serializers import OrderSerializer
from order.models import Order, order_tagger
from order.paginations import OrderPageNumberPagination
from order.constants import cooperation_status
from message.models import Message
from message.serializers import MessageSerializer


class OrdersView(ListAPIView):
    """
    get user submitted orders that logged in as a customer.
    200: payload data.
    401: unauthorized.
    token required.
    """
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = OrderSerializer
    allowed_methods = ('GET', )
    pagination_class = OrderPageNumberPagination

    def get_queryset(self):
        return Order.objects.filter(customer_id=self.request.user.id).all().order_by('-creation_date')


class OrderEditView(GenericAPIView):

    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = OrderSerializer
    allowed_methods = ('PUT', )

    def put(self, request, slug):
        """
        edit submitted order by customer. log in required.
        400: payload errors.
        404: not found.
        403: order already exists.
        200 successfully submitted.
        401: unauthorized.
        token required.
        """
        serializer = OrderSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        order = Order.objects.filter(slug=slug, customer_id=request.user.id).first()
        if not order:
            return Response(data={"message": gettext("not found")}, status=HTTP_404_NOT_FOUND)
        new_subject = serializer.validated_data.get("subject")
        if order.subject != new_subject and Order.objects.filter(subject=new_subject, customer_id=request.user.id).exists():
            return Response(data={"message": gettext("order already exists.")}, status=HTTP_403_FORBIDDEN)
        order.is_edited = True
        order.expiration_date = Order.exp_date_calculator(serializer.validated_data.get("duration"))
        serializer.update(instance=order, validated_data=serializer.validated_data)
        return Response(data={"message": gettext("successfully submitted.")}, status=HTTP_200_OK)


class ProfileView(GenericAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CustomerSerializer
    allowed_methods = ('GET', 'PUT')

    def get(self, request):
        """
        get user information that logged in as a customer.
        200: customer data.
        401: unauthorized.
        token required.
        """
        serializer = CustomerSerializer(instance=request.user)
        return Response(data=serializer.data, status=HTTP_200_OK)

    def put(self, request):
        """
        edit user information that logged in as a customer.
        200: successfully updated.
        400: payload errors.
        401: unauthorized.
        token required.
        """
        serializer = CustomerSerializer(instance=request.user, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        if serializer.validated_data.get("image") is None:
            del serializer.validated_data["image"]
        serializer.update(instance=request.user, validated_data=serializer.validated_data)
        return Response(data={"message": gettext("successfully updated.")}, status=HTTP_200_OK)


class InboxView(GenericAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = MessageSerializer
    allowed_methods = ('GET', )

    def get(self, request):
        """
        get inbox messages.
        200: payload data.
        401: unauthorized.
        token required.
        """
        customer_type = ContentType.objects.filter(model="customer").first()
        messages = Message.objects.filter(receiver_id=request.user.id, receiver_type=customer_type).all()
        serializer = MessageSerializer(instance=messages, many=True)
        return Response(data=serializer.data, status=HTTP_200_OK)


class AcceptRequestView(GenericAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    allowed_methods = ('GET', )

    def get(self, request, username, slug):
        """
        accept tagger's cooperation request for an order.
        404: order not found.
        403: invalid request.
        200: request successfully accepted.
        401: unauthorized.
        token required.
        fields: tagger's username, order's slug
        """
        order = Order.objects.filter(slug=slug, customer_id=request.user.id, isPrivate=False).first()
        if not order:
            return Response(data={"message": gettext("not found")}, status=HTTP_404_NOT_FOUND)

        order_tagger_obj = order_tagger.objects.filter(tagger__username=username, order_id=order.id).first()
        if not order_tagger_obj:
            return Response(data={"message": gettext("invalid request.")}, status=HTTP_403_FORBIDDEN)

        if order_tagger_obj.cooperation_status != cooperation_status.requested.value:
            return Response(data={"message": gettext("invalid request.")}, status=HTTP_403_FORBIDDEN)

        order_tagger_obj.cooperation_status = cooperation_status.todo.value
        order_tagger_obj.save()
        accept_message = Message(sender_obj=request.user,
                                 receiver_obj=order_tagger_obj.tagger,
                                 order_id=order.id,
                                 subject=AcceptRequestMessage.subject.value,
                                 content=AcceptRequestMessage.content.value.format(order.subject, request.user.username))
        accept_message.save()
        return Response(data={"message": gettext("request successfully accepted.")}, status=HTTP_200_OK)


class DeclineRequestView(GenericAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    allowed_methods = ('GET', )

    def get(self, request, username, slug):
        """
        decline tagger's cooperation request for an order.
        404: order not found.
        403: invalid request.
        200: request successfully declined.
        401: unauthorized.
        token required.
        fields: tagger's username, order's slug
        """
        order = Order.objects.filter(slug=slug, customer_id=request.user.id, isPrivate=False).first()
        if not order:
            return Response(data={"message": gettext("not found")}, status=HTTP_404_NOT_FOUND)

        order_tagger_obj = order_tagger.objects.filter(tagger__username=username, order_id=order.id).first()
        if not order_tagger_obj:
            return Response(data={"message": gettext("invalid request.")}, status=HTTP_403_FORBIDDEN)

        if order_tagger_obj.cooperation_status != cooperation_status.requested.value:
            return Response(data={"message": gettext("invalid request.")}, status=HTTP_403_FORBIDDEN)

        decline_message = Message(sender_obj=request.user,
                                  receiver_obj=order_tagger_obj.tagger,
                                  order_id=order.id,
                                  subject=DeclineRequestMessage.subject.value,
                                  content=DeclineRequestMessage.content.value.format(order.subject,
                                                                                   request.user.username))
        decline_message.save()
        order_tagger_obj.delete()
        return Response(data={"message": gettext("request successfully declined.")}, status=HTTP_200_OK)
