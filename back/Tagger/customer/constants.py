import enum
from django.utils.translation import gettext


class AcceptRequestMessage(enum.Enum):
    subject = gettext("Accept Request")
    content = gettext("Your request to order '{}' has been successfully accepted by '{}'")


class DeclineRequestMessage(enum.Enum):
    subject = gettext("Decline Request")
    content = gettext("Your request to order '{}' has been declined by '{}'")