from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from order.models import Order


class OrderSerializer(ModelSerializer):
    order_url = serializers.URLField(source='get_absolute_url', read_only=True)
    owner = serializers.CharField(source='customer.username', read_only=True)
    creation_datetime = serializers.DateField(source='creation_date.time', format="%H:%M:%S", read_only=True)
    expiration_date = serializers.DateField(source='expiration_date.date', read_only=True)
    creation_date = serializers.DateField(source='creation_date.date', read_only=True)

    class Meta:
        model = Order
        fields = (
            'subject',
            'content',
            'tagger_number',
            'order_url',
            'owner',
            'slug',
            'type',
            'price',
            'creation_datetime',
            'creation_date',
            'expiration_date',
        )