from rest_framework.filters import BaseFilterBackend
from django.db.models import Q
from functools import reduce
import operator


class CustomSearchFilter(BaseFilterBackend):

    def get_fields(self, view):
        return getattr(view, 'filterset_fields', None)

    def get_terms(self, request):
        return request.query_params

    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_fields(view)
        search_terms = self.get_terms(request)
        if not search_terms or not search_fields:
            return queryset

        query_list = []
        for search_term in search_terms:
            if search_term in search_fields:
                query_list.append(Q(**{search_fields[search_term]: search_terms.get(search_term)}))
        return queryset.filter(reduce(operator.and_, query_list))

