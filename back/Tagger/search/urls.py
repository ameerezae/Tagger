from django.urls import re_path, path
from .views import OrderSearchView, StatsView

urlpatterns = [
    re_path(r'^order/$', OrderSearchView.as_view(), name="search_order"),
    path('stats/', StatsView.as_view(), name="stats_view")
]
