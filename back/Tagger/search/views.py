from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.status import HTTP_200_OK
from rest_framework.response import Response
from django.db.models import Max, Min
from order.models import Order
from order.paginations import OrderPageNumberPagination
from .serializers import OrderSerializer
from .filters import CustomSearchFilter


class OrderSearchView(ListAPIView):
    """
    search on public order with subject, min_price, max_price, type, query params.
    you can use some or all pf these parameters. arrangement of parameters doesn't matter.
    if there is not parameter in request returned all orders.
    e.g: /?subject=example&min_price=1000&max_price=20000&type=example
    """
    queryset = Order.objects.filter(isPrivate=False).order_by('-creation_date')
    allowed_methods = ('GET', )
    serializer_class = OrderSerializer
    pagination_class = OrderPageNumberPagination
    filter_backends = [CustomSearchFilter]
    filterset_fields = {"subject": "subject__icontains", "min_price": "price__gte", "max_price": "price__lte", "type": "type__exact"}


class StatsView(GenericAPIView):
    def get(self, request):
        data = {'max_price': Order.objects.aggregate(Max('price')).get("price__max"),
                'min_price': Order.objects.aggregate(Min('price')).get("price__min")}
        return Response(data=data, status=HTTP_200_OK)