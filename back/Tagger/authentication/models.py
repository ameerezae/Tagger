from django.db import models
from django.contrib.auth.models import AbstractUser


class Customer(AbstractUser):
    image = models.ImageField(null=False, blank=False, default='/customer/customer_default.png', width_field="width_field", height_field="height_field")
    height_field = models.IntegerField(default=200, null=True)
    width_field = models.IntegerField(default=200, null=True)
    phone_number = models.CharField(max_length=11, blank=False)
    address = models.TextField(blank=True, null=True)


class Tagger(models.Model):
    username = models.CharField(unique=True, max_length=150)
    rate = models.IntegerField(default=0)

    def __str__(self):
        return str(self.username)
