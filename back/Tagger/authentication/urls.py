from django.urls import path
from rest_auth.views import LoginView, LogoutView
from .views import CustomerRegisterView, TaggerRegister

urlpatterns = [
    path('customer/login/', LoginView.as_view(), name="customer_login"),
    path('customer/logout/', LogoutView.as_view(), name="customer_logout"),
    path('customer/register/', CustomerRegisterView.as_view(), name="customer_register"),
    path('tagger/register/', TaggerRegister.as_view(), name="tagger_register")
]