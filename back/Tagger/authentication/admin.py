from django.contrib import admin
from .models import Customer, Tagger

admin.site.register(Customer)
admin.site.register(Tagger)
# admin.site.register(Message)