from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_403_FORBIDDEN
from rest_framework.exceptions import AuthenticationFailed
from django.utils.translation import gettext
from .serializers import CustomerRegisterSerializer
from .models import Tagger
from Tagger.settings import SSO_SECRET_KEY
import jwt


class CustomerRegisterView(GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = CustomerRegisterSerializer

    def post(self, request):
        """
        customer registration.
        200: successfully submitted.
        400: payload errors.
        """
        serializer = CustomerRegisterSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response(data={"message": gettext("successfully submitted.")}, status=HTTP_200_OK)


class TaggerRegister(GenericAPIView):
    allowed_method = ('GET', )

    def get_jwt_value(self, headers):
        auth = headers.get("Authorization").split()
        if len(auth) != 2 or auth[0] != "JWT":
            msg = gettext("Invalid token.")
            raise AuthenticationFailed(msg)
        return auth[1]

    def jwt_decode_handler(self, jwt_value):
        options = {
            'verify_exp': True,
        }
        secret_key = SSO_SECRET_KEY
        return jwt.decode(
            jwt_value,
            secret_key,
            True,
            options=options,
            leeway=0,
            audience=None,
            issuer=None,
            algorithms=['HS256']
        )

    def get_payload(self, jwt_value):
        if jwt_value is None:
            msg = gettext("Authentication credentials were not provided.")
            raise AuthenticationFailed(msg)
        try:
            payload = self.jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = gettext('Signature has expired.')
            raise AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = gettext('Error decoding signature.')
            raise AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise AuthenticationFailed()
        return payload

    def get(self, request):
        """
        tagger registration is completed in sso.just use the endpoint for register tagger user.
        403: forbidden.
        401: authentication failed.
        200: successfully submitted.
        token required.
        """
        jwt_value = self.get_jwt_value(request.headers)
        payload = self.get_payload(jwt_value)
        username = payload.get("username")
        if Tagger.objects.filter(username=username).exists():
            return Response(data={"message": gettext("You do not have permission to perform this action.")}, status=HTTP_403_FORBIDDEN)

        tagger = Tagger(username=username)
        tagger.save()
        return Response(data={"message": gettext("successfully submitted.")}, status=HTTP_200_OK)