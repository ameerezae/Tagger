from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from django.utils.translation import gettext
from django.contrib.auth.hashers import make_password
from .models import Customer


class CustomerRegisterSerializer(ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    class Meta:
        model = Customer
        fields = (
            'username',
            'email',
            'phone_number',
            'address',
            'password1',
            'password2',
        )

    def validate(self, data):
        if data.get("password1") != data.get("password2"):
            raise serializers.ValidationError(gettext("The two password fields didn't match."))
        return data

    def validate_email(self, value):
        if Customer.objects.filter(email=value).exists():
            raise serializers.ValidationError(gettext("A user is already registered with this e-mail address."))
        return value

    def save(self, **kwargs):
        data = self.validated_data
        customer = Customer(
            username=data.get("username"),
            email=data.get("email"),
            phone_number=data.get("phone_number"),
            address=data.get("address"),
            password=make_password(data.get("password1"))
        )
        customer.save()
