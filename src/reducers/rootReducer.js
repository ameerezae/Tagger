import {combineReducers } from "redux";
import {timelineReducer} from "./tagger/timelineReducer";
import {profileReducer} from "./customer/profileReducer";
import {customerInboxReducer} from "./customer/customerInboxReducer";
import {taggerInboxReducer} from "./tagger/tagger-inbox-reducer";
import {taggerProfileReducer} from "./tagger/tagger-profile-reducer";

const rootReducer = combineReducers({
    timelineReducer,
    profileReducer,
    customerInboxReducer,
    taggerInboxReducer,
    taggerProfileReducer,
});

export default rootReducer;