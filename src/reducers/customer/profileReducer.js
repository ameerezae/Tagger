import * as customerTypes from "../../actions/customer/action-types";
const initialState = {
    isProfileDetailsFetched : false,
    profileDetails : {},
    editProfile : {},
    orders : {},
    isOrdersFetched : false,
    singleOrder : {},
    singleOrderEdit : {},
    singleOrderFetched : false,


};


export function profileReducer(state = initialState, action){
    switch (action.type) {
        case customerTypes.PROFILE_TYPES.GET_PROFILE_DETAILS_SUCCESS : {
            return {...state, profileDetails: action.payload, editProfile: action.payload, isProfileDetailsFetched: true};
        }

        case customerTypes.PROFILE_TYPES.GET_CUSTOMER_ORDERS_SUCCESS : {
            return {...state, orders: action.payload, isOrdersFetched: true};
        }

        case customerTypes.PROFILE_TYPES.CLEAR_CUSTOMER_ORDERS_SUCCESS : {
            return {...state, orders: {}, isOrdersFetched: false};
        }
        
        case customerTypes.PROFILE_TYPES.EDIT_PROFILE_ITEM_SUCCESS : {
            let item = action.item; return {
                ...state,
                editProfile : {...state.editProfile, [item] : action.payload}
            }

        }

        case customerTypes.PROFILE_TYPES.CANCEL_EDIT_PROFILE_SUCCESS : {
            return {
                ...state,
                editProfile: state.profileDetails
            }
        }

        case customerTypes.PROFILE_TYPES.GET_SINGLE_ORDER_SUCCESS : {
            return {
                ...state,
                singleOrder: action.payload,
                singleOrderEdit : action.payload,
                singleOrderFetched: true,
            }
        }

        case customerTypes.PROFILE_TYPES.CHANGE_ORDER_ITEM : {
            let item = action.item
            return {
                ...state,
                singleOrderEdit: {
                    ...state.singleOrderEdit,
                    [item] : action.payload,
                }
            }
        }
        default : return state;
    }
}