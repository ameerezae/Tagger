import * as customerTypes from "../../actions/customer/action-types";

const initialState = {
    messages : [],
    isMessagesFetched : false,
};

export function customerInboxReducer(state = initialState, action){
    switch (action.type) {
        case customerTypes.CUSTOMER_INBOX_TYPES.GET_MESSAGES_SUCCESS : {
            return {...state, messages: action.payload, isMessagesFetched: true };
        }

        case customerTypes.CUSTOMER_INBOX_TYPES.CLEAR_MESSAGES_SUCCESS : {
            return {...state, messages: [], isMessagesFetched: false};
        }

        default : return state;
    }
}