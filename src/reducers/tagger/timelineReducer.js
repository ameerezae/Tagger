import * as types from "../../actions/tagger/actionTypes";

const initialState = {
    orders : null,
    isOrdersFetched : false,
    fetching : false,
    fetchingFromPage : false,
    orderDetail: null,
    isOrderDetailFetched : false,
};

export function timelineReducer(state = initialState, action){
    switch (action.type) {
        case types.TaggerTypes.GET_ORDERS_SUCCESS : return {...state, orders : action.payload, isOrdersFetched: true};
        case types.TaggerTypes.CLEAR_ORDERS_SUCCESS : return {...state, orders: null, isOrdersFetched: false};
        case types.TaggerTypes.FETCHING_SUCCESS : return {...state, fetching: action.payload};
        case types.TaggerTypes.GET_ORDERS_FROM_PAGE_SUCCESS :  {
            const newOffers = action.payload.results;
            return{
                ...state,
                orders : {
                    ...state.orders,
                    next : action.payload.next,
                    results : [...state.orders.results, ...newOffers ]

                }
            }

        }
        case types.TaggerTypes.FETCHING_FROM_PAGE_SUCCESS : return {...state, fetchingFromPage: action.payload};
        case types.TaggerTypes.GET_ORDER_DETAIL_SUCCESS : return  {...state, orderDetail: action.payload, isOrderDetailFetched: true};
        case types.TaggerTypes.CLEAR_SINGLE_ORDER_SUCCESS :return {...state, orderDetail: null,isOrderDetailFetched: false};
        default: return state;
    }
}