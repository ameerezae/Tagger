import * as taggerTypes from "../../actions/tagger/actionTypes";

const initialState = {
    messages : [],
    isMessageFetched: false,

};

export function taggerInboxReducer(state = initialState, action){
    switch (action.type) {
        case taggerTypes.ORDER_ACCEPT.GET_TAGGER_INBOX : {
            return{
                ...state,
                messages : action.payload,
                isMessageFetched: true,
            }
        }

        case taggerTypes.ORDER_ACCEPT.CLEAR_TAGGER_INBOX : {
            return {
                ...state,
                messages: [],
                isMessageFetched: false,
            }
        }

        default:return state;
    }
}