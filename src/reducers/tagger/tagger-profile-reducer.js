import * as taggerTypes from "../../actions/tagger/actionTypes";

const initialState = {
    profile: {},
    isProfileFetched : false,
};

export function taggerProfileReducer(state = initialState, action){
    switch (action.type) {
        case taggerTypes.TAGGER_PROFILE.GET_PROFILE_SUCCESS : {
            return{
                ...state,
                profile: action.payload,
                isProfileFetched: true,
            }
        }

        default : return state;
    }
}