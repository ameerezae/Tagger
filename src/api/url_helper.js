import * as mainUrls from "./main_api_urls";

class url_helper{
    static removeServerAddress (address, replace){
        return address.replace(mainUrls.TAGGER_MAIN + '/order/',replace);
    }
    static addServerAddress (owner,slug){
        return mainUrls.TAGGER_MAIN+ `/order/${owner}/${slug}/`;
    }
}

export default url_helper;