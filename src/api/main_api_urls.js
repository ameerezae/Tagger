export const TAGGER_MAIN = "https://api.armankadeh.ir/tagger";
export const SSO_MAIN = "https://api.armankadeh.ir/sso";
export const ARMANKADEH_DOMAIN = ".armankadeh.ir";
export const SSO_URL = "https://sso.armankadeh.ir";