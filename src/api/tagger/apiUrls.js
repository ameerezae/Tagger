import * as mainUrls from "../main_api_urls";


export const ORDERS = mainUrls.TAGGER_MAIN + "/order/orders/";
export const CO_OPERATE = mainUrls.TAGGER_MAIN + "/tagger/request/";
export const TAGGER_INBOX = mainUrls.TAGGER_MAIN + "/tagger/inbox/";
export const TAGGER_PROFILE = mainUrls.SSO_MAIN + "/user/profile/";