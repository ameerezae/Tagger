import * as apiUrls from "./apiUrls";
import axios from "axios";
import * as taggerConstants from "../../constants/taggerConstants";
import * as universalConstants from "../../constants/constants";
import Cookies from "js-cookie";

class TimelineApi{
    static async getCustomerOrders () {
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH_AND_QUERY(accessToken,{page:1});
            const response = await axios.get(
                apiUrls.ORDERS,
                config
            )
            if(response) {
                return response;
            }
        }catch (e) {
            if(e.response) return e.response;
        }



    }
    static async getCustomerOrdersFromPage(url){
        try {
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                url,
                config
            )
            if(response)return response
        }catch (e) {
            if(e.response) return e.response;
        }
    }

    static async getSingleCustomerOrderDetail(url){
        try {
            const response = await axios.get(
                url
            );
            if(response) return response
        }catch (e) {
            if(e.response) return e.response;
        }
    }

}
export default TimelineApi;