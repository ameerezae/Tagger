import axios from "axios";
import Cookies from "js-cookie";
import * as universalConstants from "../../constants/constants";
import * as apiUrls from "./apiUrls";
class TaggerProfile{
    static async getTaggerProfile() {
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                apiUrls.TAGGER_PROFILE,
                config,
            )
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }
}
export default TaggerProfile;