import * as apiUrls from "./apiUrls";
import axios from "axios";
import * as universalConstants from "../../constants/constants";
import Cookies from "js-cookie";

class OrderAcceptApi{
    static async sendCoOperate(owner, slug){
        try {
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                apiUrls.CO_OPERATE + owner + "/" + slug + "/",
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }

    static async getTaggerInbox(){
        try {
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                apiUrls.TAGGER_INBOX,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }
}
export default OrderAcceptApi;