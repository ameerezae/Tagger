import axios from "axios";
import Cookies from "js-cookie";
import * as universalConstants from "../../constants/constants";
import * as CustomerApiUrls from "./customer_api_urls";
class CreateOrderApi{
    static async submitPost(post){
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken)
            const response = await axios.post(
                CustomerApiUrls.CUSTOMER_CREATE_ORDER,
                post,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }
}

export default CreateOrderApi;