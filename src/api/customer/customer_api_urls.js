import * as mainUrls from "../main_api_urls";

export const CUSTOMER_CREATE_ORDER = mainUrls.TAGGER_MAIN + "/order/submit/";
export const CUSTOMER_LOGIN = mainUrls.TAGGER_MAIN + "/auth/customer/login/";
export const CUSTOMER_REGISTER = mainUrls.TAGGER_MAIN + "/auth/customer/register/";
export const CUSTOMER_PROFILE = mainUrls.TAGGER_MAIN + "/customer/profile/";
export const CUSTOMER_ORDERS = mainUrls.TAGGER_MAIN + "/customer/orders/";
export const EDIT_CUSTOMER_PROFILE = mainUrls.TAGGER_MAIN + "/customer/profile/";
export const EDIT_CUSTOMER_ORDER = mainUrls.TAGGER_MAIN + "/customer/orders/";
export const MESSAGES = mainUrls.TAGGER_MAIN + "/customer/inbox/";
export const ACCEPT_REQUEST = mainUrls.TAGGER_MAIN + "/customer/accept/request/";
export const DECLINE_REQUEST = mainUrls.TAGGER_MAIN + "/customer/decline/request/";