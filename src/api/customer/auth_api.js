import * as universalConstants from "../../constants/constants.js";
import axios from "axios";
import  * as customerApiUrls from "../customer/customer_api_urls";

class Auth_api{
    static async submitAuthCredentials (credentials){
        try {
            const config = {
                headers: universalConstants.HEADERS_WITHOUT_AUTH,
            };

            const body = JSON.stringify(credentials);
            const response = await axios.post(
                customerApiUrls.CUSTOMER_LOGIN,
                body,
                config
            );
            if(response) {
                return response;
            }
        }catch (e) {
            if(e.response){
                return e.response;
            }
        }
    }
    static async submitRegisterCredentials (credentials){
        try{
            const config = {
                headers: universalConstants.HEADERS_WITHOUT_AUTH,
            };

            const body = JSON.stringify(credentials);
            const response = await axios.post(
                customerApiUrls.CUSTOMER_REGISTER,
                body,
                config
            );
            if(response) {
                return response;
            }
        }catch (e) {
            if(e.response){
                return e.response;
            }
        }
    }
}

export default Auth_api;