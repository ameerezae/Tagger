import axios from "axios";
import Cookies from "js-cookie";
import * as universalConstants from "../../constants/constants";
import * as infoCons from "../../constants/customer/info-constants";
import * as CustomerApiUrls from "./customer_api_urls";

class ProfileApi {
    static async getProfileDetails() {
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                CustomerApiUrls.CUSTOMER_PROFILE,
                config
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }
    static async getCustomerOrders() {
        try {
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                CustomerApiUrls.CUSTOMER_ORDERS,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }


    static async EditCustomerProfile(edited){
        try {
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            let formData = new FormData();
            Object.keys(infoCons.PROFILE).forEach((key)=>{
                if((infoCons.PROFILE[key] === infoCons.PROFILE.IMAGE &&
                    typeof(edited[infoCons.PROFILE[key]]) === "string") ||
                    (infoCons.PROFILE[key] === infoCons.PROFILE.ADDRESS &&
                        edited[infoCons.PROFILE[key]] === "null")
                ){
                    formData.append(infoCons.PROFILE[key], "");
                }else{
                    formData.append(infoCons.PROFILE[key],edited[infoCons.PROFILE[key]])
                }
            });

            const response = await axios.put(
                CustomerApiUrls.EDIT_CUSTOMER_PROFILE,
                formData,
                config
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }


    static async getSingleOrderDetail(url){
        try {
            const response = await axios.get(
                url
            );
            if(response) return response
        }catch (e) {
            if(e.response) return e.response;
        }
    }

    static async editPost(post, slug){
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken)
            const response = await axios.put(
                CustomerApiUrls.EDIT_CUSTOMER_ORDER + slug + "/",
                post,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response;
        }
    }
}


export default ProfileApi