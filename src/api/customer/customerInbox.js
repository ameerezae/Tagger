import axios from "axios";
import Cookies from "js-cookie";
import * as universalConstants from "../../constants/constants";
import * as CustomerApiUrls from "./customer_api_urls";


class CustomerInboxApi{
    static async getCustomerMessages (){
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                CustomerApiUrls.MESSAGES,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response
        }
    }

    static async customerAcceptRequest(username, slug){
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                `${CustomerApiUrls.ACCEPT_REQUEST}${username}/${slug}/`,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response
        }

    }

    static async customerDeclineRequest(username, slug){
        try{
            const accessToken = Cookies.get(universalConstants.ACCESS_TOKEN);
            const config = universalConstants.CONFIG_WITH_AUTH(accessToken);
            const response = await axios.get(
                `${CustomerApiUrls.DECLINE_REQUEST}${username}/${slug}/`,
                config,
            );
            if(response) return response;
        }catch (e) {
            if(e.response) return e.response
        }

    }
}
export default CustomerInboxApi