import * as customerTypes from "./action-types";
import * as universalConstants from "../../constants/constants";
import ProfileApi from "../../api/customer/profile-api";
import M from "materialize-css";
export const getProfileDetails = () => {
    return async function(dispatch){
        const response = await ProfileApi.getProfileDetails();
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK) {
            dispatch(dispatchGetProfileDetails(response.data))
        }else{
            M.toast({html: response.data.detail , classes : "white-text red vazir-font-medium"});
        }
    }
};
const dispatchGetProfileDetails = (profile) => ({type : customerTypes.PROFILE_TYPES.GET_PROFILE_DETAILS_SUCCESS, payload : profile});



export const getCustomerOrders = () => {
    return async function(dispatch){
        const response = await ProfileApi.getCustomerOrders();
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetCustomerOrders(response.data))
        }else{
            M.toast({html: response.data.detail , classes : "white-text red vazir-font-medium"});
        }
    }
};

const dispatchGetCustomerOrders = (orders) => ({type : customerTypes.PROFILE_TYPES.GET_CUSTOMER_ORDERS_SUCCESS, payload : orders});

export const clearCustomerOrders = () => {
    return function (dispatch) {
        dispatch(dispatchClearCustomerOrders())
    }
};
const dispatchClearCustomerOrders = () => ({type : customerTypes.PROFILE_TYPES.CLEAR_CUSTOMER_ORDERS_SUCCESS})

//Change the profile details in reducer state
export const changeProfileItem = (item ,value) => {
    return function(dispatch){
        dispatch(dispatchChangeProfile(item, value))
    }
};
export const dispatchChangeProfile = (item,value) => ({type : customerTypes.PROFILE_TYPES.EDIT_PROFILE_ITEM_SUCCESS, payload : value, item: item});


export const getSingleOrderDetail = (url) => {
    return async function (dispatch){
        const response = await ProfileApi.getSingleOrderDetail(url);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetSingleOrderDetail(response.data));
        }
    }
}

const dispatchGetSingleOrderDetail = (detail) => ({type: customerTypes.PROFILE_TYPES.GET_SINGLE_ORDER_SUCCESS ,payload:detail});


export const changeOrderItem = (item ,value) => {
    return function(dispatch){
        dispatch(dispatchChangeOrderItem(item, value))
    }
};
const dispatchChangeOrderItem = (item,value) => ({type : customerTypes.PROFILE_TYPES.CHANGE_ORDER_ITEM, payload : value, item: item});


// cancel change profile items
export const cancelChangeProfile = () => {
    return function(dispatch) {
        dispatch(dispatchCancelChangeProfile())
    }
} ;
const dispatchCancelChangeProfile = () => ({type : customerTypes.PROFILE_TYPES.CANCEL_EDIT_PROFILE_SUCCESS});