import * as customerTypes from "./action-types";
import * as universalConstants from "../../constants/constants";
import CustomerInboxApi from "../../api/customer/customerInbox";
import M from "materialize-css";

export const getCustomerInbox = () => {
    return async function(dispatch){
        const response = await CustomerInboxApi.getCustomerMessages();
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetCustomerInbox(response.data))
        }
        else{
            // M.toast({html: response.data.detail , classes : "white-text red vazir-font-medium"});
        }
    }
};
const dispatchGetCustomerInbox = (inbox) => ({type : customerTypes.CUSTOMER_INBOX_TYPES.GET_MESSAGES_SUCCESS , payload :inbox})



export const clearCustomerInbox = () => {
    return function (dispatch) {
        dispatch(dispatchClearCustomerInbox())
    }
};
const dispatchClearCustomerInbox = () => ({type : customerTypes.CUSTOMER_INBOX_TYPES.CLEAR_MESSAGES_SUCCESS});

export const acceptRequest = (username, slug) => {
    return async function(){
        const response = await CustomerInboxApi.customerAcceptRequest(username, slug);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            M.toast({html: response.data.message , classes : "white-text green vazir-font-medium"});
        }
        else{
            const error = response.data.detail ? response.data.detail : response.data.message;
            M.toast({html: error , classes : "white-text red vazir-font-medium"});
        }
    }
};

export const declineRequest = (username, slug) => {
    return async function(){
        const response = await CustomerInboxApi.customerDeclineRequest(username, slug);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            M.toast({html: response.data.message , classes : "white-text green vazir-font-medium"});
        }
        else{
            const error = response.data.detail ? response.data.detail : response.data.message;
            M.toast({html: error , classes : "white-text red vazir-font-medium"});
        }
    }
};