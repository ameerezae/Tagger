import * as types from "./actionTypes";
import * as universalConstants from "../../constants/constants";
import TimelineApi from "../../api/tagger/timelineApi";
import {TaggerTypes} from "./actionTypes";

export const getCustomerOrders = () => {
    return async function(dispatch){
        dispatch(dispatchFetching(true));
        const response = await TimelineApi.getCustomerOrders();
        if(!response) throw new Error("I CRASHED :(");
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetCustomerOrders(response.data));
            console.log("FETCHING FINISHED");
            dispatch(dispatchFetching(false));
        };
    }

}
const dispatchGetCustomerOrders = (orders) => ({type: types.TaggerTypes.GET_ORDERS_SUCCESS ,payload:orders});
const dispatchFetching = (bool) => ({type : types.TaggerTypes.FETCHING_SUCCESS, payload : bool});

export const clearReducer = () => {
    return function (dispatch) {
        dispatch(dispatchClearReducer())
    }
};

const dispatchClearReducer = () => ({type: types.TaggerTypes.CLEAR_ORDERS_SUCCESS});

export const getCustomerOrdersFromPage = (url) => {
    return async function (dispatch){
        dispatch(dispatchFetchingFromPage(true));
        const response = await TimelineApi.getCustomerOrdersFromPage(url);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetCustomerOrdersFromPage(response.data));
            dispatch(dispatchFetchingFromPage(false));
        }

    }
}

const dispatchGetCustomerOrdersFromPage = (orders) => ({type : types.TaggerTypes.GET_ORDERS_FROM_PAGE_SUCCESS, payload : orders });
const dispatchFetchingFromPage = (bool) => ({type : types.TaggerTypes.FETCHING_FROM_PAGE_SUCCESS, payload:bool});


export const getSingleCustomerOrderDetail = (url) => {
    return async function (dispatch){
        const response = await TimelineApi.getSingleCustomerOrderDetail(url);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetSingleCustomerOrderDetail(response.data));
        }
    }
}

const dispatchGetSingleCustomerOrderDetail = (detail) => ({type:types.TaggerTypes.GET_ORDER_DETAIL_SUCCESS, payload:detail});

export const clearSingleOrderDetail = () => {
    return function(dispatch){
        dispatch(dispatchClearSingleOrderDetail());
    }
};
const dispatchClearSingleOrderDetail = () => ({type: types.TaggerTypes.CLEAR_SINGLE_ORDER_SUCCESS});