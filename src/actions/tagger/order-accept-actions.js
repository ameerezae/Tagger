import OrderAcceptApi from "../../api/tagger/order-accept-api";
import * as universalConstants from "../../constants/constants";
import M from "materialize-css";
import * as taggerTypes from "./actionTypes";


export const sendCoOperateRequest = (owner, slug) => {
    return async function(){
        const response = await OrderAcceptApi.sendCoOperate(owner, slug);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            M.toast({html: response.data.message , classes : "white-text green vazir-font-medium"});
        }
        else{
            const error = response.data.detail ? response.data.detail : response.data.message;
            M.toast({html: error , classes : "white-text red vazir-font-medium"});
        }
    }
};


export const getTaggerInbox = () => {
    return async function(dispatch){
        const response = await OrderAcceptApi.getTaggerInbox();
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetTaggerInbox(response.data));
        }else{
            const error = response.data.detail ? response.data.detail : response.data.message;
            M.toast({html: error , classes : "white-text red vazir-font-medium"});
        }
    }
};

const dispatchGetTaggerInbox = (inbox) => ({type: taggerTypes.ORDER_ACCEPT.GET_TAGGER_INBOX , payload: inbox});

//clear tagger inbox
export const clearTaggerInbox = () => {
    return function (dispatch) {
        dispatch(dispatchClearTaggerInbox());
    }
};
const dispatchClearTaggerInbox = () => ({type : taggerTypes.ORDER_ACCEPT.CLEAR_TAGGER_INBOX});