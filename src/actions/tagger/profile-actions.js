import * as taggerTypes from "./actionTypes";
import TaggerProfile from "../../api/tagger/tagger-profile";
import * as universalCons from "../../constants/constants";
import M from "materialize-css";

export const getTaggerProfile = () => {
    return async function(dispatch){
        const response = await TaggerProfile.getTaggerProfile();
        if(response.status === universalCons.REQUESTS_STATUS_NUMBERS.OK){
            dispatch(dispatchGetTaggerProfile(response.data));
        }else{
            const error = response.data.detail ? response.data.detail : response.data.message;
            M.toast({html: error , classes : "white-text red vazir-font-medium"});
        }
    }
};
const dispatchGetTaggerProfile = (profile) => ({type : taggerTypes.TAGGER_PROFILE.GET_PROFILE_SUCCESS, payload : profile});