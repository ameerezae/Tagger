import React from 'react';
import './App.css';
import {Switch, Route, Router} from "react-router-dom";
import createHistory from 'history/createBrowserHistory';
import Tagger from "./containers/tagger/tagger";
import Customer from "./containers/customer/customer";
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize.min';
import Authentication from "./containers/customer/authentication/authentication";
import 'material-design-icons/iconfont/material-icons.css';
const history = createHistory();
function App() {
  return (
      <Router history={history}>
        <Switch>
          <Route path="/tagger" component={Tagger}/>
          <Route path="/customer" component={Customer}/>
          <Route exact path="/" component={Authentication}/>
        </Switch>
      </Router>
  );
}

export default App;
