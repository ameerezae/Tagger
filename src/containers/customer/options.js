import React from 'react';
import {Route, Switch} from "react-router-dom";
import CreateOrder from "./create-order/create-order";
import Information from "./information/information";
import Orders from "./information/orders/orders";
import DetailedOrders from "./information/orders/detailedOrders";
import OrderEdit from "./information/orders/order-edit";
import Inbox from "./inbox/inbox";
import NotFound from "../../components/404/not-found";
const Options = () => {
    return (
        <Switch>
            <Route exact path="/customer/info" component={Information}/>
            <Route exact path="/customer/create-order" component={CreateOrder}/>
            <Route exact path="/customer/my-orders" component={Orders}/>
            <Route exact path="/customer/my-orders/:owner/:slug/" component={DetailedOrders}/>
            <Route exact path="/customer/my-orders/:owner/:slug/edit" component={OrderEdit}/>
            <Route exact path="/customer/inbox" component={Inbox}/>
            <Route exact component={NotFound}/>
        </Switch>
    );
};

export default Options;