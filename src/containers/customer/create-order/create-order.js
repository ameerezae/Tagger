import React, {Component} from 'react';
import CreateOrderView from "./create-order-view";
import CreateOrderApi from "../../../api/customer/create_order_api";
import * as universalConstants from "../../../constants/constants";
import * as CRConstants from "../../../constants/customer/create-order-constants";
import M from "materialize-css";
import {Row} from "react-materialize";
class CreateOrder extends Component {

    componentWillUnmount() {
        this.clearPostState();
    }

    state = {
        cashNegative : false,
        timeNegative : false,
        taggerNegative : false,
        post : {
            [CRConstants.ORDERS.SUBJECT] : "",
            [CRConstants.ORDERS.CONTENT]: "",
            [CRConstants.ORDERS.TAGGER]: null,
            [CRConstants.ORDERS.TYPE]: "",
            [CRConstants.ORDERS.PRIVATE]: false,
            [CRConstants.ORDERS.PRICE]: 0,
            [CRConstants.ORDERS.DURATION]: 0
        },
        createOrderErrors : {},
    };


    checkIfNegativeNumber = (id,number) => {
        if(number < 0){
            this.setState({[id]: true});
        }else{
            this.setState({[id]: false});
        }
    };

    async handleSubmit(event,post){
        event.preventDefault();
        const response = await CreateOrderApi.submitPost(post);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.CREATED) {
            M.toast({html: response.data.message, classes : "white-text green vazir-font-medium"})
        }else{
            this.setState({createOrderErrors : response.data})
        }
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState(prevState => {
            const newState = {...prevState};
            newState.post[name] = value;
            newState.createOrderErrors={};
            return newState;
        })
    };

    clearPostState = () => {
        this.setState(prevState => {
            const newState = {...prevState};
            newState.post = {
                [CRConstants.ORDERS.SUBJECT] : "",
                [CRConstants.ORDERS.CONTENT]: "",
                [CRConstants.ORDERS.TAGGER]: null,
                [CRConstants.ORDERS.TYPE]: "",
                [CRConstants.ORDERS.PRIVATE]: false,
                [CRConstants.ORDERS.PRICE]: 0,
                [CRConstants.ORDERS.DURATION]: 0
            };
            return newState;
        })
    };


    render() {
        return (
            <div className=" mt30 mr30 ml30">
                <Row className="directionRightToLeft valign-wrapper">
                    <h6 className="iran-sans-font mr30">
                        مراحل ثبت سفارش
                    </h6>
                </Row>
                <CreateOrderView
                    cashNegative = {this.state.cashNegative}
                    timeNegative = {this.state.timeNegative}
                    taggerNegative = {this.state.taggerNegative}
                    checkIfNegativeNumber = {this.checkIfNegativeNumber}
                    handleChange = {this.handleChange}
                    handleSubmit={(event)=>this.handleSubmit(event,this.state.post)}
                    values = {this.state.post}
                    Errors = {this.state.createOrderErrors}
                    Button = {"ثبت سفارش"}

                />
            </div>

        );
    }
}

export default CreateOrder;