import React from 'react';
import {Col, Row, TextInput, Icon, Select ,Textarea, Button} from "react-materialize";
import "../scss/customer.scss";
import * as CRConstants from "../../../constants/customer/create-order-constants";
import NumberFormat from 'react-number-format';
import * as Constants from "../../../constants/customer/create-order-constants";
const CreateOrderView = (props) => {
    return (
        <Col >
            <Row ClassName="mt50">
                <Col s={12} m={12} l={10} offset="l1" className="mt30">
                    <Row>
                        <Col l={6} m={6} s={12} push="l6 m6 ">
                            <Row className="valign-wrapper directionRightToLeft black-grey-text mt20">
                                <Col className="circle-icon-number">1</Col>
                                <Col className="vazir-font-medium">
                                    چه نوع سفارشی دارید؟
                                </Col>
                            </Row>
                            <Row>
                                <Select
                                    label=" انتخاب کنید"
                                    icon={<Icon>local_offer</Icon>}
                                    options={{
                                        dropdownOptions: {
                                            classes: "vazir-font-medium",
                                            alignment: 'right',
                                            closeOnClick: true,
                                            constrainWidth: true,
                                            container: null,
                                            coverTrigger: true,
                                            hover: false,
                                            inDuration: 150,
                                            onCloseEnd: null,
                                            onCloseStart: null,
                                            onOpenEnd: null,
                                            onOpenStart: null,
                                            outDuration: 250
                                        }
                                    }}
                                    value={props.values[Constants.ORDERS.TYPE]}
                                    name={Constants.ORDERS.TYPE}
                                    onChange={(event)=>{props.handleChange(event)}}
                                    validate
                                    error = {props.Errors[Constants.CO_ERRORS.TYPE] ? props.Errors[Constants.CO_ERRORS.TYPE][0] : null}
                                    selectClassName={props.Errors[Constants.CO_ERRORS.TYPE] ? "vazir-font-medium black-grey-text invalid" :
                                        "vazir-font-medium black-grey-text"}

                                >
                                    <option
                                        disabled
                                        value=""
                                    >
                                        انتخاب کنید
                                    </option>
                                    <option value="textual">
                                        متنی
                                    </option>
                                    <option value="sound">
                                        صوتی
                                    </option>
                                </Select>
                            </Row>
                        </Col>
                        <Col l={6} m={6} s={12} pull="l6 m6">
                            <Row className="valign-wrapper directionRightToLeft black-grey-text mt20">
                                <Col className="circle-icon-number">2</Col>
                                <Col className="vazir-font-medium">
                                    عنوان سفارش خود را بنویسید.
                                </Col>
                            </Row>
                            <Row className="vazir-font-medium">
                                <TextInput
                                    validate
                                    icon={<Icon>subject</Icon>}
                                    label="عنوان"
                                    value={props.values[Constants.ORDERS.SUBJECT]}
                                    className={props.Errors[Constants.CO_ERRORS.SUBJECT] ? "iran-sans-font directionRightToLeft create-order-font-size-input invalid" :
                                        "iran-sans-font directionRightToLeft create-order-font-size-input"}
                                    error = { props.Errors[Constants.CO_ERRORS.SUBJECT] ? props.Errors[Constants.CO_ERRORS.SUBJECT][0] : null}
                                    name={Constants.ORDERS.SUBJECT}
                                    onChange={(event)=>{
                                        props.handleChange(event)
                                    }}
                                />
                            </Row>
                        </Col>
                    </Row>

                    <Row className="valign-wrapper directionRightToLeft mt20 black-grey-text">
                        <Col className="circle-icon-number">3</Col>
                        <Col className="vazir-font-medium">
                            سفارش خودرا توضیح دهید.
                        </Col>
                    </Row>
                    <Row className="vazir-font-medium">
                        <Textarea
                            validate
                            label="توضیحات"
                            icon={<Icon>create</Icon>}
                            value={props.values[Constants.ORDERS.CONTENT]}
                            className={props.Errors[Constants.CO_ERRORS.CONTENT] ?
                                "iran-sans-font directionRightToLeft create-order-font-size-input invalid"
                            : "iran-sans-font directionRightToLeft create-order-font-size-input"}
                            error = {props.Errors[Constants.CO_ERRORS.CONTENT] ? props.Errors[Constants.CO_ERRORS.CONTENT][0] : null}
                            name={Constants.ORDERS.CONTENT}
                            onChange={(event)=>{
                                props.handleChange(event)
                            }}
                        />
                    </Row>
                    <Row>
                        <Col l={6} m={6} s={12} push="l6 m6">
                            <Row className="valign-wrapper directionRightToLeft black-grey-text">
                                <Col className="circle-icon-number">4</Col>
                                <Col className="vazir-font-medium">
                                    بودجه مورد نظر شما چقدر است؟
                                </Col>
                            </Row>
                            <Row className="vazir-font-medium valign-wrapper ">
                                <NumberFormat
                                    customInput={TextInput}
                                    validate
                                    type="text"
                                    className={ props.cashNegative ? "yekan-font  invalid center directionRightToLeft create-order-font-size-input" : "yekan-font directionRightToLeft center create-order-font-size-input"}
                                    error={ props.cashNegative ? CRConstants.NEGATIVE_NUMBER_ERROR : null}
                                    label="بودجه"
                                    icon={<Icon>credit_card</Icon>}
                                    min="0"
                                    suffix={"    ریال"}
                                    name={Constants.ORDERS.PRICE}
                                    onValueChange={(values)=>{
                                        props.handleChange({
                                            target : {
                                                name : "price",
                                                value: values.floatValue
                                            }
                                        });
                                        props.checkIfNegativeNumber("cashNegative",values.floatValue)
                                    }}
                                    thousandSeparator={true}
                                    value={props.values[Constants.ORDERS.PRICE]}
                                />
                            </Row>
                        </Col>
                        <Col l={6} m={6} s={12} pull="l6 m6">
                            <Row className="valign-wrapper directionRightToLeft black-grey-text">
                                <Col className="circle-icon-number">5</Col>
                                <Col className="vazir-font-medium">
                                    زمان مورد انتظار شما چقدر است؟(روز)
                                </Col>
                            </Row>
                            <Row className="vazir-font-medium">
                                <TextInput
                                    validate
                                    className={ props.timeNegative || props.Errors[Constants.CO_ERRORS.DURATION] ? "yekan-font create-order-font-size-input  invalid center" : "create-order-font-size-input yekan-font  center"}
                                    error={ props.timeNegative ? CRConstants.NEGATIVE_NUMBER_ERROR :
                                        props.Errors[Constants.CO_ERRORS.DURATION] ? props.Errors[Constants.CO_ERRORS.DURATION][0]  :null}
                                    label="زمان"
                                    icon={<Icon>timelapse</Icon>}
                                    type="number"
                                    min="0"
                                    name={Constants.ORDERS.DURATION}
                                    onChange={(event)=>{
                                        props.handleChange(event);
                                        props.checkIfNegativeNumber("timeNegative",event.target.value)
                                    }}
                                    value={props.values[Constants.ORDERS.DURATION]}
                                />
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col l={6} m={6} s={12} offset="l6 m6">
                            <Row className="valign-wrapper directionRightToLeft black-grey-text">
                                <Col className="circle-icon-number">6</Col>
                                <Col className="vazir-font-medium">
                                    چند نفر روی پروژه فعالیت کنن؟
                                </Col>
                            </Row>
                            <Row className="vazir-font-medium">
                                <TextInput
                                    validate
                                    className={ props.taggerNegative ? "yekan-font create-order-font-size-input  invalid center" : "create-order-font-size-input yekan-font  center"}
                                    error={ props.taggerNegative ? CRConstants.NEGATIVE_NUMBER_ERROR : null}

                                    label="تعداد"
                                    icon={<Icon>group</Icon>}
                                    type="number"
                                    min="0"
                                    name={Constants.ORDERS.TAGGER}
                                    onChange={(event)=>{
                                        props.handleChange(event);
                                        props.checkIfNegativeNumber("taggerNegative",event.target.value)
                                    }}
                                    value={props.values[Constants.ORDERS.TAGGER]}
                                />
                            </Row>
                        </Col>
                    </Row>
                    <Row className="center-align">
                        <Button
                            type="submit"
                            className="vazir-font-medium create-order-submit-button mb50 mt20"
                            onClick={props.handleSubmit}
                            disabled={props.timeNegative || props.cashNegative || props.taggerNegative}
                        >{props.Button}</Button>
                    </Row>

                </Col>
            </Row>
        </Col>
    );
};

export default CreateOrderView;