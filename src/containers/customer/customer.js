import React, {Component} from 'react';
import {Col} from "react-materialize";
import CustomerNavbar from "./navbar/navbar";
import Tabs from "./tabs/tabs";
import * as universalConstants from "../../constants/constants";
import Cookies from "js-cookie";
import Options from "./options";
import M from "materialize-css";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getProfileDetails} from "../../actions/customer/profile-actions";

class Customer extends Component {
    componentWillMount() {
        if(!this.isAuthorized()) {
            M.toast({html: 'ابتدا وارد شوید', classes : "white-text red vazir-font-medium"});
            this.props.history.push("/");
        }else{
            this.props.getProfileDetails();
        }
    }

    isAuthorized = () => {
        const token = Cookies.get(universalConstants.ACCESS_TOKEN);
        return !!token;
    };


    render() {
        return (
            <Col>
                <CustomerNavbar/>
                <Tabs/>
                <Options/>
            </Col>
        );
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProfileDetails,
    },dispatch)
}

export default connect(null,mapDispatchToProps)(Customer);