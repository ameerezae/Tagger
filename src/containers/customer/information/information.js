import React, {Component} from 'react';
import {Col, Row} from "react-materialize";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getProfileDetails} from "../../../actions/customer/profile-actions";
import Profile from "./profile/profile";
import LoadingScreen from "../../../components/loading-screen/loading-screen";
import ProfileButtons from "./profile/profile-buttons";
import EditProfile from "./profile/edit-profile";
import * as infoCons from "../../../constants/customer/info-constants";

class Information extends Component {
    state = {
        showEditProfile: false,
    };

    componentWillMount() {
        this.props.getProfileDetails();
    }

    toggleShowEdit = () => {
        this.setState({showEditProfile: !this.state.showEditProfile})
    };

    render() {
        return (
            <div>
                <div className="mr50 ml50 mt50 right-align">
                    <h5 className="vazir-font-medium grey-text-custom">اطلاعات شخصی</h5>
                </div>
                <div className="mr50 ml50 mt30 right-align">
                    {this.props.profileState.isProfileDetailsFetched ?
                        <div>
                            <Row>
                                <Col s={12} m={8} l={9}  push="l3 m4">

                                    {this.state.showEditProfile ?
                                        <EditProfile
                                            toggleShowEdit={this.toggleShowEdit}
                                        />
                                        :
                                        <Profile
                                            image = {this.props.profileState.profileDetails[infoCons.PROFILE.IMAGE]}
                                            firstName = {this.props.profileState.profileDetails[infoCons.PROFILE.FIRST_NAME]}
                                            lastName = {this.props.profileState.profileDetails[infoCons.PROFILE.LAST_NAME]}
                                            username = {this.props.profileState.profileDetails[infoCons.PROFILE.USERNAME]}
                                            phone = {this.props.profileState.profileDetails[infoCons.PROFILE.PHONE_NUMBER]}
                                            email = {this.props.profileState.profileDetails[infoCons.PROFILE.EMAIL]}
                                            address = {this.props.profileState.profileDetails[infoCons.PROFILE.ADDRESS]}
                                        />
                                    }
                                </Col>
                                <Col s={12} m={4} l={3} pull="l9 m8">
                                    <ProfileButtons
                                        toggleShowEdit={this.toggleShowEdit}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt70 center-align">
                                <img width="90%" className="responsive-img "
                                     src="https://ponisha.ir/defaults/cover/default.png" alt=""/>
                            </Row>
                        </div>
                        :
                        <LoadingScreen/>
                    }
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProfileDetails,
    }, dispatch)

}


export default connect(mapStateToProps, mapDispatchToProps)(Information);