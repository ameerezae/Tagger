import React from 'react';
import {Button, Row, Icon} from "react-materialize";
import {NavLink} from "react-router-dom";

const OrderButtons = (props) => {
    return (
        <div className="center vazir-font-medium mb50">
            <Row>
                <NavLink to={"edit"}>
                    <Button
                        node="button"
                        small
                        flat
                        className="vazir-font-medium order-button-edit"
                    >
                        ویرایش
                        <Icon left>
                            edit
                        </Icon>
                    </Button>
                </NavLink>
            </Row>
            <Row className="mt10">
                <Button
                    node="button"
                    small
                    flat
                    className="vazir-font-medium order-button-delete"
                >
                    حذف
                    <Icon left>
                        clear
                    </Icon>
                </Button>
            </Row>
        </div>
    );
};

export default OrderButtons;