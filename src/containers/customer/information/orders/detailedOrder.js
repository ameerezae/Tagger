import React from 'react';
import {Button, Card, Col, Icon, Row} from "react-materialize";
import BlueTick from "../../../../assets/png/Blue-tick-png.png";
import NumberFormat from "react-number-format";
import {Link} from "react-router-dom";
import "./orders.scss";
import OrderButtons from "./orderButtons";

const DetailedOrder = (props) => {
    return (
        <Col>
            <Row>
                <Col push=" l4" s={12} m={12} l={8}>
                    <Row>
                        <Col offset="l1" s={12} m={12} l={9} className="right-align">
                            <h5 className="iran-sans-font black-grey-text  directionRightToLeft">{props.subject}</h5>
                            <Col className="valign-wrapper right iran-sans-font black-grey-text mt20 directionRightToLeft">
                                <Icon className="ml10">date_range</Icon>
                                تاریخ ایجاد :
                                &nbsp;&nbsp;
                                <div className="Btitr-font ">{props.creationDate.replace("-","/").replace("-","/")}</div>
                            </Col>
                        </Col>
                        <Col l={2} className="right-align hide-on-med-and-down">
                            <img src={BlueTick} width="100px" alt="logo"/>
                        </Col>
                    </Row>

                    <hr className="hr-text vazir-font-medium mb20" data-content="اطلاعات"/>

                    <Row className="directionRightToLeft vazir-font-medium order-details">
                        <Col s={12} m={4} l={4}>
                            <Row className="valign-wrapper Btitr-font">
                                <Icon className="ml10">info</Icon>
                                <p className="iran-sans-font">نوع سفارش    :</p>
                                &nbsp;&nbsp;
                                <p>{props.type}</p>
                            </Row>
                        </Col>
                        <Col s={12} m={4} l={4} className="center-align">
                            <Row className="valign-wrapper Btitr-font">
                                <Icon className="ml10">person</Icon>
                                <p className="iran-sans-font">تعداد تگرها :</p>
                                &nbsp;&nbsp;
                                <p>{props.tagger_no}</p>
                            </Row>
                        </Col>
                        <Col s={12} m={4} l={4} className="center-align">
                            <Row className="valign-wrapper Btitr-font">
                                <Icon className="ml10">credit_card</Icon>
                                <p className="iran-sans-font"> بودجه     :</p>
                                &nbsp;&nbsp;
                                <NumberFormat value={props.price} displayType={'text'} thousandSeparator={true} />
                                &nbsp;&nbsp;
                                <p className="iran-sans-font"> ریال</p>
                            </Row>
                        </Col>
                    </Row>
                    <hr className="hr-text vazir-font-medium mt20" data-content="توضیحات"/>
                    <Row className="directionRightToLeft mt50">
                        <div className="iran-sans-font black-grey-text pr20 pl20 pb30 white-space-prewrap tagger-content-line-height">
                            {props.content}
                        </div>
                    </Row>
                </Col>

                <Col pull="l8" s={12} m={12} l={3}>

                    <Row >
                        <Card
                            actions={[
                                <Row>
                                    <Col s={12} m={12} l={12}>
                                        <Col className="valign-wrapper right iran-sans-font  directionRightToLeft">
                                            <div className="Btitr-font ">{props.expireDate.replace("-","/").replace("-","/")}</div>
                                        </Col>
                                    </Col>
                                </Row>,

                            ]}
                            className=""
                            textClassName=" vazir-font-bold  directionRightToLeft valign-wrapper center"
                        >
                            <Row className="valign-wrapper">
                                <Icon className="ml10">date_range</Icon>
                                مهلت   :
                            </Row>

                        </Card>
                    </Row>
                    <Row  className="mt50">
                        <Col s={12} l={12}>
                            <OrderButtons/>
                        </Col>
                    </Row>

                </Col>

            </Row>
        </Col>
    );
};

export default DetailedOrder;