import React, {Component} from 'react';
import CreateOrderView from "../../create-order/create-order-view";
import {connect} from "react-redux";
import {Row} from "react-materialize";
import * as universalConstants from "../../../../constants/constants";
import M from "materialize-css";
import ProfileApi from "../../../../api/customer/profile-api";
import {bindActionCreators} from "redux";
import {getSingleOrderDetail, changeOrderItem} from "../../../../actions/customer/profile-actions";
import url_helper from "../../../../api/url_helper";
class OrderEdit extends Component {
    componentWillMount() {
        this.props.getSingleOrderDetail(url_helper.addServerAddress(this.props.match.params.owner,this.props.match.params.slug))
    }


    state = {
        cashNegative : false,
        timeNegative : false,
        taggerNegative : false,
        post : this.props.profileState.singleOrderEdit,
        editOrderErrors : {},
    };

    checkIfNegativeNumber = (id,number) => {
        if(number < 0){
            this.setState({[id]: true});
        }else{
            this.setState({[id]: false});
        }
    };

    async handleSubmit(event,post){
        event.preventDefault();
        const response = await ProfileApi.editPost(post, this.props.match.params.slug);
        if(response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK) {
            M.toast({html: response.data.message, classes : "white-text green vazir-font-medium"})
            this.props.history.goBack();
        }else{
            this.setState({editOrderErrors : response.data})
        }
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.props.changeOrderItem(name, value);
    };
    render() {
        return (
            <div className=" mt30 mr30 ml30">
                <Row className="directionRightToLeft valign-wrapper">
                    <h6 className="iran-sans-font mr30">
                        ویرایش سفارش
                    </h6>
                </Row>
                <CreateOrderView
                    cashNegative = {this.state.cashNegative}
                    timeNegative = {this.state.timeNegative}
                    taggerNegative = {this.state.taggerNegative}
                    checkIfNegativeNumber = {this.checkIfNegativeNumber}
                    handleChange = {this.handleChange}
                    handleSubmit={(event)=>this.handleSubmit(event,this.props.profileState.singleOrderEdit)}
                    values = {this.props.profileState.singleOrderEdit}
                    Errors = {this.state.editOrderErrors}
                    Button = {"ویرایش"}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getSingleOrderDetail,
        changeOrderItem,
    }, dispatch)

}
export default connect(mapStateToProps,mapDispatchToProps)(OrderEdit);