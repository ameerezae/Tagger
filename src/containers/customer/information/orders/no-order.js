import React from 'react';
import {Col, Row} from "react-materialize";
import "./orders.scss";

const NoOrder = () => {
    return (
            <Col s={12} l={10} offset="l1" className="center mt40">
                <div className="no-order">
                    <img src="https://img.icons8.com/dotty/80/000000/return-purchase.png"/>
                    <div className="iran-sans-font grey-text-custom">سفارش  جدیدی موجود نیست</div>
                </div>
            </Col>
    );
};

export default NoOrder;