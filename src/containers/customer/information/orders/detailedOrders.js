import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getSingleOrderDetail} from "../../../../actions/customer/profile-actions"
import url_helper from "../../../../api/url_helper";
import {Col} from "react-materialize";
import DetailedOrder from "./detailedOrder";
import * as infoCons from "../../../../constants/customer/info-constants";
class DetailedOrders extends Component {
    componentWillMount() {
        this.props.getSingleOrderDetail(url_helper.addServerAddress(this.props.match.params.owner,this.props.match.params.slug))
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.profileState.singleOrderFetched) this.setTitleOfPage(this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.SUBJECT])
    }

    setTitleOfPage = (title) => {
        if(title){
            return document.title = title;
        }
    };
    render() {
        return (
            <Col className="mr50 ml50 mt40 ">
                {this.props.profileState.singleOrderFetched ?
                    <DetailedOrder
                        subject={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.SUBJECT]}
                        price={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.PRICE]}
                        tagger_no={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.TAGGER_NO]}
                        type={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.TYPE]}
                        content={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.CONTENT]}
                        creationDate = {this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.CREATION_DATE]}
                        expireDate={this.props.profileState.singleOrder[infoCons.SINGLE_ORDER.EXP_DATE]}
                    />
                :null}
            </Col>
        );
    }
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getSingleOrderDetail
    }, dispatch)

}

export default connect(mapStateToProps,mapDispatchToProps)(DetailedOrders);