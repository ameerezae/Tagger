import React, {Component} from 'react';
import Offer from "../../../tagger/timeline/offers/offer";
import {connect} from "react-redux";
import {Row} from "react-materialize"
import * as taggerCons from "../../../../constants/taggerConstants";
import * as infoCons from "../../../../constants/customer/info-constants"
import {bindActionCreators} from "redux";
import {getCustomerOrders, clearCustomerOrders} from "../../../../actions/customer/profile-actions";
import url_helper from "../../../../api/url_helper";
import LoadingScreen from "../../../../components/loading-screen/loading-screen";
import NoOrder from "./no-order";

class Orders extends Component {
    componentWillMount() {
        this.props.getCustomerOrders();
    }

    componentWillUnmount() {
        this.props.clearCustomerOrders()
    }

    render() {
        return (
            <div className="mr50 ml50 mt40 ">
                {this.props.profileState.isOrdersFetched ?
                    <Row>
                        <Row>
                            <h6 className="grey-text vazir-font-medium right ">پیشنهادات من</h6>
                        </Row>
                        {this.props.profileState.orders[taggerCons.ORDERS.RESULTS].length !== 0 ?
                            this.props.profileState.orders[taggerCons.ORDERS.RESULTS].map((order, key) => {
                                return (
                                    <Offer
                                        key={key}
                                        to={url_helper.removeServerAddress(order[infoCons.ORDERS.URL], "my-orders/")}
                                        subject={order[infoCons.ORDERS.SUBJECT]}
                                        content={order[infoCons.ORDERS.CONTENT]}
                                        type={order[infoCons.ORDERS.TYPE]}
                                        taggerNo={order[infoCons.ORDERS.TAGGER]}
                                        price={order[infoCons.ORDERS.PRICE]}
                                        creationDate={order[infoCons.ORDERS.CREATION_DATE]}
                                        expirationDate={order[infoCons.ORDERS.EXP_DATE]}

                                    />
                                )
                            })
                            : <NoOrder/>}
                    </Row>
                    : <LoadingScreen/>}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCustomerOrders,
        clearCustomerOrders
    }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(Orders);