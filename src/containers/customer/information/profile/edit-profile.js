import React, {Component} from 'react';
import EditProfileView from "./edit-profile-view";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {changeProfileItem, getProfileDetails, cancelChangeProfile} from "../../../../actions/customer/profile-actions";
import ProfileApi from "../../../../api/customer/profile-api";
import * as universalCons from "../../../../constants/constants";
import * as infoCons from "../../../../constants/customer/info-constants"
import M from "materialize-css";
class EditProfile extends Component {
    state = {
        editErrors : {},
    };
    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.props.changeProfileItem(name,value);
        this.setState({editErrors : {}})
    };
    async handleSubmit(event,edited){
        event.preventDefault();
        const response = await ProfileApi.EditCustomerProfile(edited);
        if(response.status === universalCons.REQUESTS_STATUS_NUMBERS.OK){
            M.toast({html: response.data.message, classes : "white-text green vazir-font-medium"})
            this.props.getProfileDetails();
            this.props.toggleShowEdit();
        }
        else{
            this.setState({editErrors : response.data})
        }
    }

    render() {
        return (
            <EditProfileView
                editData = {this.props.profileState.editProfile}
                handleChange = {this.handleChange}
                handleSubmit = {(event)=>this.handleSubmit(event,this.props.profileState.editProfile)}
                first_name_errors = {this.state.editErrors[infoCons.PROFILE.FIRST_NAME]}
                last_name_errors = {this.state.editErrors[infoCons.PROFILE.LAST_NAME]}
                address_errors = {this.state.editErrors[infoCons.PROFILE.ADDRESS]}
                phone_errors = {this.state.editErrors[infoCons.PROFILE.PHONE_NUMBER]}
                toggleShowEdit = {this.props.toggleShowEdit}
                cancelChangeProfile = {this.props.cancelChangeProfile}
            />
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        changeProfileItem,
        getProfileDetails,
        cancelChangeProfile,
    },dispatch);
}


function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);