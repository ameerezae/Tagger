import React, {Component} from 'react';
import Avatar from 'react-avatar-edit';
import {Button, Modal, Row, Col, Icon} from "react-materialize";
import "../information.scss";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {changeProfileItem, getProfileDetails} from "../../../../actions/customer/profile-actions";
import * as infoCons from "../../../../constants/customer/info-constants";


class AvatarCropper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            preview: null,
        };
        this.onCrop = this.onCrop.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onBeforeFileLoad = this.onBeforeFileLoad.bind(this)
    }

    onClose() {
        this.setState({preview: null})
    }

    async onCrop(preview) {
        const imageFile = this.dataURLtoFile(preview);
        this.props.changeProfileItem("image",imageFile);
    }

    dataURLtoFile(dataUrl) {

        var arr = dataUrl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], "image.png", {type:mime});
    }

    onBeforeFileLoad(elem) {
        if(elem.target.files[0].size  < 6144){
            alert("عکس مورد نظر بسیار حجیم است.");
            elem.target.value = "";
        };
    }

    render () {
        return (
            <Row className="vazir-font-medium center">
                <Modal
                    actions={[
                        <Row className="center-align">
                            <Button flat modal="close" node="button" className="vazir-font-medium white-text custom-green-bg" style={{width:"20%"}} waves="green">تایید</Button>
                        </Row>
                    ]}
                    trigger={
                        <div>
                            <img width="120px"
                                 src={this.props.profileState.editProfile[infoCons.PROFILE.IMAGE]}
                                 className="circle " alt=""/>
                            <Icon className="centered-icon white-text">add_circle_outline</Icon>
                        </div>
                    }
                >
                        <Row>
                            <Col s={10} l={5} offset="s1 l3" className="center-align center avatar-cropper vazir-font-medium grey-text-custom">
                                <Avatar
                                    width={300}
                                    height={270}
                                    onCrop={this.onCrop}
                                    onClose={this.onClose}
                                    label="انتخاب کنید"
                                    className="vazir-font-medium grey-text-custom"
                                />
                            </Col>
                            <Col l={4} className="right-align hide-on-small-only">
                                <span className="vazir-font-medium grey-text-custom">انتخاب کنید</span>
                            </Col>
                        </Row>

                </Modal>
            </Row>
        )
    }
}


function mapDispatchToProps(dispatch){
    return bindActionCreators({
        changeProfileItem,
        getProfileDetails
    },dispatch);
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AvatarCropper);