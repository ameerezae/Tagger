import React from 'react';
import {Button, Col, Row, TextInput} from "react-materialize";
import * as infoCons from "../../../../constants/customer/info-constants";
import AvatarCropper from "./avatar-cropper";

const EditProfileView = (props) => {
    return (
        <div>
            <Row className="vazir-font-medium directionRightToLeft" >
                <Col s={12} m={3} l={2} className="center-align mb20" push="l9 m9" >
                    <AvatarCropper/>
                </Col>
                <Col s={12} m={9} l={7} offset="l2" pull="l2 m3">
                    <Row className="right-align valign-wrapper" >
                        <Col l={6}>
                            <TextInput
                                label="نام"
                                value={props.editData[infoCons.PROFILE.FIRST_NAME]}
                                name={infoCons.PROFILE.FIRST_NAME}
                                onChange={(event) => props.handleChange(event)}
                                validate
                                className={props.first_name_errors ? "invalid vazir-font-medium grey-text-custom" : "vazir-font-medium grey-text-custom"}
                                error={props.first_name_errors
                                    ? props.first_name_errors[0] : null}
                            />
                        </Col>
                        <Col l={6}>
                            <TextInput
                                label="نام خانوادگی"
                                value={props.editData[infoCons.PROFILE.LAST_NAME]}
                                name={infoCons.PROFILE.LAST_NAME}
                                onChange={(event) => props.handleChange(event)}
                                validate
                                className={props.last_name_errors ? "invalid vazir-font-medium grey-text-custom" : "vazir-font-medium grey-text-custom"}
                                error={props.last_name_errors
                                    ? props.last_name_errors[0] : null}
                            />
                        </Col>
                    </Row>
                    <Row className="right-align valign-wrapper">
                        <Col s={12} l={12}>
                            <TextInput
                                label="تلفن"
                                type="tel"
                                value={props.editData[infoCons.PROFILE.PHONE_NUMBER]}
                                name={infoCons.PROFILE.PHONE_NUMBER}
                                onChange={(event) => props.handleChange(event)}
                                validate
                                className={props.phone_errors ? "invalid vazir-font-medium grey-text-custom" : "vazir-font-medium grey-text-custom"}
                                error={props.phone_errors
                                    ? props.phone_errors[0] : null}
                            />
                        </Col>
                    </Row>
                    <Row className="right-align valign-wrapper">
                        <Col s={12} l={12}>
                            <TextInput
                                label="آدرس"
                                value={props.editData[infoCons.PROFILE.ADDRESS]}
                                name={infoCons.PROFILE.ADDRESS}
                                onChange={(event) => props.handleChange(event)}
                                validate
                                className={props.address_errors ? "invalid vazir-font-medium grey-text-custom" : "vazir-font-medium grey-text-custom"}
                                error={props.address_errors
                                    ? props.address_errors[0] : null}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="center-align mb40">
                <Button
                    type="submit"
                    onClick={props.handleSubmit}
                    className="vazir-font-bold submit-button-edit mr10"
                    node="button"
                    flat
                >
                    ذخیره
                </Button>
                <Button
                    type="submit"
                    onClick={()=> {
                        props.toggleShowEdit();
                        props.cancelChangeProfile();
                    }}
                    className="vazir-font-bold cancel-button-edit"
                    node="button"
                    flat
                >
                   انصراف
                </Button>
            </Row>
        </div>
    );
};

export default EditProfileView;