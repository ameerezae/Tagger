import React from 'react';
import {Button, Row} from "react-materialize";
import {NavLink} from "react-router-dom";
import "../information.scss";
import * as universalCons from "../../../../constants/constants";
import Cookies from "js-cookie";

const ProfileButtons = (props) => {
    return (
        <div className="center vazir-font-medium">
            <Row>
                <NavLink to={"/customer/create-order"}>
                    <Button
                        className="submit-order-button"
                        node="a"
                        small
                        flat
                    >
                        ثبت سفارش
                    </Button>
                </NavLink>
            </Row>
            <Row className="mt10">
                <Button
                    className="edit-profile-button"
                    node="a"
                    small
                    flat
                    onClick={
                        () => props.toggleShowEdit()
                    }
                >
                    ویرایش پروفایل
                </Button>
            </Row>
            <Row className="mt10">
                <Button
                    href="/"
                    node="a"
                    small
                    flat
                    className="exit-button"
                    onClick={()=>{Cookies.remove(universalCons.ACCESS_TOKEN)}}
                >
                    خروج
                </Button>
            </Row>
        </div>
    );
};

export default ProfileButtons;