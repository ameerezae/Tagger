import React from 'react';
import "../../scss/customer.scss";
import "../information.scss";
import {Col, Row} from "react-materialize";

const Profile = (props) => {
    return (
        <Row className="mb40">
            <Col l={2} s={12} m={2} className="center-align mb20" push="m9 l9">
                <img width="120px"
                     src={props.image}
                     className="circle " alt=""/>
            </Col>
            <Col l={9} s={12} m={9} pull="m2 l2">
                <Row className="right-align valign-wrapper">
                    <Col s={6} l={6} offset="l1">
                        <Row className="valign-wrapper">
                            <Col s={12} m={12} l={6} className="center-on-small-only">
                                <span
                                    className="grey-text-custom vazir-font-medium">{props.firstName}&nbsp;{props.lastName}</span>
                            </Col>
                            <Col l={6} className="hide-on-med-and-down">
                                <span className="vazir-font-medium">نام و نام خانوادگی</span>
                            </Col>
                        </Row>

                    </Col>
                    <Col l={1} className="right-align hide-on-small-only">
                        <span className="grey-text-custom">|</span>
                    </Col>
                    <Col s={6} l={4}>
                        <Row className="valign-wrapper">
                            <Col s={12} m={12} l={6} className="center-on-small-only">
                                <span
                                    className="grey-text-custom username-style">@{props.username}</span>
                            </Col>
                            <Col l={6} className="hide-on-med-and-down">
                                <span className="vazir-font-medium">نام کاربری</span>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="right-align mt20">
                    <Col s={6} l={6} offset="l1">
                        <Row className="valign-wrapper">
                            <Col s={12} m={12} l={6} className="center-on-small-only">
                                <span
                                    className="grey-text-custom vazir-font-medium">{props.phone}</span>
                            </Col>
                            <Col l={6} className="hide-on-med-and-down">
                                <span className="vazir-font-medium">تلفن</span>
                            </Col>
                        </Row>
                    </Col>
                    <Col l={1} className="right-align hide-on-med-and-down">
                        <span className="grey-text-custom">|</span>
                    </Col>
                    <Col s={6} l={4}>
                        <Row className="valign-wrapper">
                            <Col s={12} m={12} l={6} className="center-on-small-only">
                                <span
                                    className="grey-text-custom vazir-font-medium">{props.email}</span>
                            </Col>
                            <Col l={6} className="hide-on-med-and-down">
                                <span className="vazir-font-medium">ایمیل</span>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="right-align mt20">
                    <Col s={12} l={11} className="center-on-small-only">
                           <span
                               className="grey-text-custom vazir-font-medium">{props.address}</span>

                    </Col>
                    <Col l={1} className="hide-on-med-and-down">
                        <span className="vazir-font-medium">آدرس</span>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};

export default Profile;
