import React from 'react';
import {Collapsible, CollapsibleItem, Icon, Row, Col} from "react-materialize";
import {NavLink} from "react-router-dom";
import "./navbar.scss"

const SmallDeviceNavbar = (props) => {
    return (
        <div className=" hide-on-med-and-up">
            <div className="vazir-font-medium">
                <Collapsible accordion className="mobile-collapsible" >
                    <CollapsibleItem node="div" >
                        <NavLink
                            to={"/customer/info"}
                            onClick={()=>props.closeNav()}
                        >
                            <Row className="valign-wrapper">
                                <Col s={2} className="center-align ">
                                    <Icon>timeline</Icon>
                                </Col>
                                <Col s={10} className="right-align ">
                                    <span>اطلاعات</span>
                                </Col>
                            </Row>
                        </NavLink>
                        <hr/>
                        <NavLink
                            to={"/customer/create-order"}
                            onClick={()=>props.closeNav()}
                        >
                            <Row className="valign-wrapper">
                                <Col s={2} className="center-align ">
                                    <Icon>mode_edit</Icon>
                                </Col>
                                <Col s={10} className="right-align ">
                                    <span>ایجاد سفارش</span>
                                </Col>
                            </Row>
                        </NavLink>
                        <hr/>
                        <NavLink
                            to={"/customer/inbox"}
                            onClick={()=>props.closeNav()}
                        >
                            <Row className="valign-wrapper">
                                <Col s={2} className="center-align ">
                                    <Icon>message</Icon>
                                </Col>
                                <Col s={10} className="right-align">
                                    <span>پیام ها</span>
                                </Col>
                            </Row>
                        </NavLink>
                        <hr/>
                        <NavLink
                            to={"/customer/my-orders"}
                            onClick={()=>props.closeNav()}
                        >
                            <Row className="valign-wrapper">
                                <Col s={2} className="center-align ">
                                    <Icon>publish</Icon>
                                </Col>
                                <Col s={10} className="right-align ">
                                    <span>سفارشات</span>
                                </Col>
                            </Row>
                        </NavLink>
                        <hr/>
                        <a href="/" onClick={()=>props.logUserOut}>
                            <Row className="valign-wrapper">
                                <Col s={2} className="center-align ">
                                    <Icon>exit_to_app</Icon>
                                </Col>
                                <Col s={10} className="right-align ">
                                    <span>خروج</span>
                                </Col>
                            </Row>
                        </a>
                    </CollapsibleItem>
                </Collapsible>
            </div>
        </div>
    );
};

export default SmallDeviceNavbar;