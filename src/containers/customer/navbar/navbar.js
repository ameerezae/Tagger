import React, {Component} from 'react';
import {Icon, Row, Col,} from "react-materialize";
import * as universalConstants from "../../../constants/constants";
import Cookies from "js-cookie";
import {connect} from "react-redux";
import * as infoCons from "../../../constants/customer/info-constants";
import LOGO from "../../../assets/png/32-336319_tag-icon-svg-hd-png-download.png";
import "./navbar.scss";
import M from "materialize-css";
import Messages from "./messages";
import SmallDeviceNavbar from "./small-device-navbar";

class CustomerNavbar extends Component {
    state = {
        expanded : false,
    };

    collapseNavbar = () =>{
        this.setState({expanded : !this.state.expanded},

            ()=>{
                const elems = document.querySelector('.collapsible');
                const instance = M.Collapsible.getInstance(elems);
                if(this.state.expanded ) {
                    instance.close(0)
                }
                else {
                    instance.open(0)
                }
            });
    };

    logUserOut = () => {
        Cookies.remove(universalConstants.ACCESS_TOKEN)
    };

    render() {
        return (
            <div>
                <div className="navbar-fixed">
                    <nav className="tagger-navbar-color vazir-font-medium">
                        <div className=" nav-wrapper">
                            <ul id="nav-mobile" className="left hide-on-small-only">
                                <li>
                                    <a href="/" onClick={()=>this.logUserOut()}>
                                        <Row className="vertical_align">
                                            <Col s={6} className="">
                                                {' '}خروج
                                            </Col>
                                            <Col s={6}>
                                                <Icon className="">
                                                    exit_to_app
                                                </Icon>
                                            </Col>
                                        </Row>
                                    </a>
                                </li>
                                <li>
                                    <Messages/>
                                </li>
                            </ul>
                            <ul className="right">

                                {this.props.profileState.isProfileDetailsFetched ?
                                    <li className="">
                                        <a href="/customer/info">
                                            {`@ ${this.props.profileState.profileDetails[infoCons.PROFILE.USERNAME]}`}
                                        </a>
                                    </li>
                                    : null
                                }
                                <li><img src={LOGO} className="navbar-logo" alt="logo"/></li>
                            </ul>
                            <ul className="hide-on-med-and-up show-on-small vazir-font-medium">
                                <li onClick={()=>this.collapseNavbar()}><a><Icon>menu</Icon></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <SmallDeviceNavbar
                    logUserOut = {this.logUserOut}
                    closeNav = {this.collapseNavbar}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        profileState: state.profileReducer,
    }
}

export default connect(mapStateToProps)(CustomerNavbar);
