import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Divider, Dropdown, Icon} from "react-materialize";

class Messages extends Component {
    render() {
        return (
            <a>
                <Dropdown
                    // style={{whiteSpace: "nowrap",position:"absolute",width:"200px",  top: "64px", left: "76px", opacity: "1"}}
                    // style={{display:"block"}}
                    options={{
                        alignment: 'left',
                        constrainWidth : true,
                        autoTrigger: true,
                        closeOnClick: true,
                        container: "div",
                        hover: false,
                        inDuration: 150,
                        onCloseEnd: null,
                        onCloseStart: null,
                        onOpenEnd: null,
                        onOpenStart: null,
                        outDuration: 250
                    }}
                    trigger={<div><Icon>message</Icon></div>}
                    className="dropdown-messages"
                >
                    <a href="#">
                        one
                    </a>
                    <Divider/>
                    <a href="#">
                        two
                    </a>
                    <a href="#">
                        three
                    </a>
                    <a href="#">
                        <Icon>
                            view_module
                        </Icon>
                        four
                    </a>
                    <a href="#">
                        <Icon>
                            cloud
                        </Icon>
                        {' '}five
                        lorem ipson
                    </a>
                </Dropdown>
            </a>
        );
    }
}
function mapStateToProps(state) {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    },dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Messages);