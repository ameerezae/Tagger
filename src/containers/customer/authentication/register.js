import React, {Component} from 'react';
import RegisterView from "./register_view";
import Auth_api from "../../../api/customer/auth_api";
import * as universalConstants from "../../../constants/constants";
import * as authConstants from "../../../constants/customer/customer_auth_constants";
import M from "materialize-css";

class Register extends Component {
    state = {
        credentials: {
            username : "",
            email: "",
            password1: "",
            password2 : "",
            phone_number : "",
        },
        registerErrors: {},
        showPreLoader : false,
    };

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState(prevState => {
            const newState = {...prevState};
            newState.registerErrors = {};
            newState.credentials[name] = value;
            return newState;
        })
    };

    async handleSubmit(event,credentials) {
        event.preventDefault();
        this.togglePreLoader();
        const response = await Auth_api.submitRegisterCredentials(credentials);
        if (response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK) {
            M.toast({html: 'شما با موفقیت ثبت نام شدید', classes : "white-text green vazir-font-medium"})
        }
        else {
            this.setState({registerErrors: response.data});
        }
        this.togglePreLoader();
    }
    togglePreLoader = () =>{
        this.setState({showPreLoader : !this.state.showPreLoader});
    };

    render() {
        return (
            <RegisterView
                handleChange = {(event)=>this.handleChange(event)}
                handleSubmit = {(event) => {this.handleSubmit(event,this.state.credentials)}}
                usernameErrors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.USERNAME]}
                emailErrors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.EMAIL]}
                password1Errors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.PASS1]}
                password2Errors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.PASS2]}
                phoneErrors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.PHONE]}
                non_field_errors = {this.state.registerErrors[authConstants.REGISTER_ERRORS.NON_FIELD]}
                showPreLoader = {this.state.showPreLoader}
            />
        );
    }
}

export default Register;