import React from 'react';
import {Button, Col, Preloader, Row, TextInput} from "react-materialize";

const RegisterView = (props) => {
    return (
        <form>
            <Row className="vazir-font-medium mt40">
                <Col s={12} m={12} l={12}>
                    <TextInput
                        icon="fingerprint"
                        label="نام کاربری"
                        name="username"
                        onChange={props.handleChange}
                        validate
                        className={props.usernameErrors ? "invalid" : null}
                        error={props.usernameErrors
                            ? props.usernameErrors[0] : null}
                    />
                </Col>
            </Row>
            <Row className="vazir-font-medium">
                <Col s={12} m={12} l={12}>
                    <TextInput
                        icon="person_outline"
                        label="ایمیل"
                        email
                        name="email"
                        onChange={props.handleChange}
                        validate
                        className={props.emailErrors ? "invalid" : null}
                        error={props.emailErrors
                            ? props.emailErrors[0] : null}
                    />
                </Col>
            </Row>
            <Row className="vazir-font-medium">
                <Col s={12} m={6} l={6}>
                    <TextInput
                        icon="lock_open"
                        label="رمزعبور"
                        password
                        name="password1"
                        onChange={props.handleChange}
                        validate
                        className={props.non_field_errors || props.password1Errors ? "invalid" : null}
                        error={props.non_field_errors
                            ? props.non_field_errors[0] :
                            props.password1Errors
                            ? props.password1Errors[0] : null}
                    />
                </Col>
                <Col s={12} m={6} l={6}>
                    <TextInput
                        icon="lock_outline"
                        label="تکرار رمزعبور"
                        password
                        name="password2"
                        onChange={props.handleChange}
                        validate
                        className={props.password2Errors ? "invalid" : null}
                        error={props.password2Errors
                            ? props.password2Errors[0] : null}

                    />
                </Col>
            </Row>
            <Row className="vazir-font-medium">
                <Col s={12} m={12} l={12}>
                    <TextInput
                        icon="phone_iphone"
                        label="شماره همراه"
                        phone
                        name="phone_number"
                        onChange={props.handleChange}
                        validate
                        className={props.phoneErrors ? "invalid" : null}
                        error={props.phoneErrors
                            ? props.phoneErrors[0] : null}
                    />
                </Col>
            </Row>
            <Row className="center mt40 mb20">
                <Button
                    type="submit"
                    onClick={props.handleSubmit}
                    className="vazir-font-bold green darken-2"
                    node="button"
                    style={{
                        marginRight: '5px',
                        width:"80%",
                        borderRadius:"25px",

                    }}
                    waves="light"
                >
                    <Row>
                        ثبت نام
                        {props.showPreLoader ?
                            <Preloader
                                active
                                color="blue"
                                flashing
                                className="small right preloader-position"
                            />
                            : null}
                    </Row>
                </Button>
            </Row>
        </form>
    );
};

export default RegisterView;