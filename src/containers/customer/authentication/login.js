import React, {Component} from 'react';
import {Col, Row, TextInput, Button, Checkbox, Preloader} from "react-materialize";
import * as authConstants from "../../../constants/customer/customer_auth_constants";
import Auth_api from "../../../api/customer/auth_api";
import * as universalConstants from "../../../constants/constants";
import Cookies from "js-cookie";
import LoginView from "./login_view";

class Login extends Component {
    state = {
        credentials: {
            email: "",
            password: "",
        },
        authErrors: {},
        showPreLoader : false,
        rememberMe : false,
    };

    componentWillMount() {
        this.setRememberedCredentials();
    };

    setRememberedCredentials = () => {
      const isChecked = Cookies.get("checked");
      if(isChecked){
          const email = Cookies.get("email");
          const password = Cookies.get("password");
          this.setState(prevState => {
              const newState = {...prevState};
              newState.credentials.email = email;
              newState.credentials.password = password;
              newState.rememberMe = isChecked;
              return newState;
          })
      }
    };

    togglePreLoader = () =>{
        this.setState({showPreLoader : !this.state.showPreLoader});
    };

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState(prevState => {
            const newState = {...prevState};
            newState.authErrors = {};
            newState.credentials[name] = value;
            return newState;
        })
    };

    async handleSubmit(event,credentials) {
        event.preventDefault()
        this.togglePreLoader();
        const response = await Auth_api.submitAuthCredentials(credentials);
        if (response.status === universalConstants.REQUESTS_STATUS_NUMBERS.OK){
            Cookies.set(universalConstants.ACCESS_TOKEN, response.data.token);
            this.rememberMe(this.state.rememberMe);
            this.props.history.push("/customer/info")
        }
        else {
            this.setState({authErrors: response.data})
        }
        this.togglePreLoader();

    }

    toggleRememberMe = (value) => {
        this.setState({rememberMe : value});
    };

    rememberMe = (isChecked) => {
        if(isChecked){
            Cookies.set("checked",this.state.rememberMe);
            Cookies.set("email",this.state.credentials.email);
            Cookies.set("password",this.state.credentials.password);
        }else{
            Cookies.remove("checked");
            Cookies.remove("email");
            Cookies.remove("password");
        }
    };


    render() {
        return (
            <LoginView
                non_field_errors = {this.state.authErrors[authConstants.LOGIN_ERRORS.NON_FIELD]}
                emailErrors = {this.state.authErrors[authConstants.LOGIN_ERRORS.EMAIL]}
                handleChange = {(event)=>this.handleChange(event)}
                passwordErrors = {this.state.authErrors[authConstants.LOGIN_ERRORS.PASSWORD]}
                showPreLoader = {this.state.showPreLoader}
                handleSubmit={(event) => {this.handleSubmit(event,this.state.credentials)}}
                toggleRememberMe = {this.toggleRememberMe}
                email = {this.state.credentials.email}
                password = {this.state.credentials.password}
                rememberMe = {this.state.rememberMe}
            />
        );
    }
}

export default Login;