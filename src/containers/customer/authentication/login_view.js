import React from 'react';
import {Button, Checkbox, Col, Preloader, Row, TextInput} from "react-materialize";

const LoginView = (props) => {
    return (
        <div>
            <form>
                <Row className="vazir-font-medium mt40">
                    <TextInput
                        validate
                        className={ props.non_field_errors ||
                        props.emailErrors
                            ? "invalid" : null}
                        error={ props.non_field_errors
                            ? props.non_field_errors[0] :
                            props.emailErrors
                                ? props.emailErrors[0] : null}
                        name="email"
                        onChange={props.handleChange}
                        icon="person_outline"
                        label="ایمیل"
                        email
                        value = {props.email}
                    />
                </Row>
                <Row className="vazir-font-medium">
                    <TextInput
                        validate
                        className={props.passwordErrors ? "invalid" : null}
                        error={props.passwordErrors
                            ? props.passwordErrors[0] : null}
                        name="password"
                        onChange={props.handleChange}
                        icon="lock_outline"
                        label="رمزعبور"
                        password
                        value={props.password}

                    />
                </Row>
                <Row className="directionRightToLeft mt10 ">
                    <Col s={12} m={12} l={12} className="right-align valign-wrapper">
                        <span className="iran-sans-font ">مرا بخاطر بسپار.</span>
                        <Checkbox
                            onChange = {(event)=>props.toggleRememberMe(event.target.checked)}
                            checked = {props.rememberMe}
                        />
                    </Col>
                </Row>
                <Row className="center mt40">
                    <Button
                        type="submit"
                        onClick={props.handleSubmit}
                        className="vazir-font-bold green darken-2"
                        node="button"
                        style={{
                            marginRight: '5px',
                            width: "85%",
                            borderRadius: "25px",
                            background: "#e00026",

                        }}
                        waves="light"
                    >
                        <Row>
                            ورود
                            {props.showPreLoader ?
                                <Preloader
                                    active
                                    color="yellow"
                                    className="small right preloader-position"
                                />
                                : null}
                        </Row>
                    </Button>
                </Row>
                <Row className="mt40 directionRightToLeft">
                    <Col s={6} m={6} l={6} className="left-align">
                        <span className="iran-sans-font ">همین الان ثبت نام کنید.</span>
                    </Col>
                    <Col s={6} m={6} l={6} className="right-align">
                        <span className="iran-sans-font ">فراموشی رمز عبور؟</span>
                    </Col>
                </Row>
            </form>
        </div>
    );
};

export default LoginView;