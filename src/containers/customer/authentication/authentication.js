import React, {Component} from 'react';
import {Col, Row, Tab, Tabs} from "react-materialize";
import "./authentication.scss";
import Login from "./login";
import Register from "./register";

class Authentication extends Component {
    render() {
        return (
            <Col className="auth-main">
                <Col className="authentication-background"></Col>
                <Row className="valign-wrapper" style={{minHeight: "100vh"}}>

                    <Col offset="l4 s1 m3" l={4} m={6} s={10} className="auth-container ">
                        <Col s={12} m={12} l={12} className="auth-tab">
                            <Tabs className="tab-demo  no-background tabs-fixed-width vazir-font-medium">
                                <Tab
                                    style={{color: "rgb(224, 0, 38)"}}
                                    options={{
                                        duration: 300,
                                        onShow: null,
                                        responsiveThreshold: Infinity,
                                        swipeable: false
                                    }}
                                    title="ورود"
                                >
                                    <Login history={this.props.history}/>
                                </Tab>
                                <Tab
                                    style={{color: "rgb(224, 0, 38)"}}
                                    options={{
                                        duration: 300,
                                        onShow: null,
                                        responsiveThreshold: Infinity,
                                        swipeable: false
                                    }}
                                    title="ثبت نام"
                                >
                                    <Register/>
                                </Tab>
                            </Tabs>

                        </Col>
                    </Col>


                </Row>
            </Col>
        );
    }
}

export default Authentication;