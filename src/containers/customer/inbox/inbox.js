import React, {Component} from 'react';
import InboxMessages from "./messages/inbox-messages";
import {Row} from "react-materialize";
import "./inbox.scss";
class Inbox extends Component {
    render() {
        return (
            <div className=" mt30 mr30 ml30">
                <Row className="directionRightToLeft valign-wrapper">
                    <h6 className="iran-sans-font mr30 grey-text-custom">
                        پروژه ها
                    </h6>
                </Row>
                <div className="mt20 ">
                    <InboxMessages/>
                </div>
            </div>

        );
    }
}

export default Inbox;