import React from 'react';
import {Button} from "react-materialize";
import "../inbox.scss";

const InboxMessage = (props) => {
    return (
        <tr className="iran-sans-font directionRightToLeft">
            <td >
                <Button
                    flat
                    className="iran-sans-font accept-request-button"
                    onClick={props.acceptRequest}
                >
                    پذیرش
                </Button>
                <Button
                    flat
                    className="iran-sans-font decline-request-button"
                    onClick={props.declineRequest}
                >
                    رد
                </Button>
            </td>
            <td>
                {props.sender}
            </td>
            <td>
                {props.offerType}
            </td>
            <td>
                {props.projectName}
            </td>
        </tr>
    );
};

export default InboxMessage;