import React from 'react';
import "../inbox.scss";
const NoMessage = () => {
    return (
        <tr className="no-border">
            <td colSpan="4" className="dataTables_empty">
                <div className="no-result">
                    <img src="https://img.icons8.com/cotton/64/000000/mailing.png" alt="mail"/>
                    <div className="iran-sans-font">پیام جدیدی دریافت نشده است</div>
                </div>
            </td>
        </tr>
    );
};

export default NoMessage;