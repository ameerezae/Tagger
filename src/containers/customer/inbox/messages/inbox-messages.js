import React, {Component} from 'react';
import {Col, Row, Table} from "react-materialize";
import {bindActionCreators} from "redux";
import {getCustomerInbox, clearCustomerInbox} from "../../../../actions/customer/customer-inbox-actions";
import {connect} from "react-redux";
import InboxMessage from "./inbox-message";
import * as inboxCons from "../../../../constants/customer/customer_inbox_constants";
import {acceptRequest, declineRequest} from "../../../../actions/customer/customer-inbox-actions";
import LoadingScreen from "../../../../components/loading-screen/loading-screen";
import NoMessage from "./no-message";

class InboxMessages extends Component {
    componentWillMount() {
        this.props.getCustomerInbox()
    }

    componentWillUnmount() {
        this.props.clearCustomerInbox();
    }

    render() {
        return (
            <Row>
                <Col s={12} l={10} offset="l1">
                    {this.props.customerInboxState.isMessagesFetched ?

                        <Table>
                            <thead>
                            <tr className="vazir-font-medium">
                                <th data-field="id">
                                    عملیات
                                </th>
                                <th data-field="price">
                                    فرستنده
                                </th>
                                <th data-field="name">
                                    پیشنهاد
                                </th>
                                <th data-field="id">
                                    نام پروژه
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.customerInboxState.messages.length !== 0 ?
                                this.props.customerInboxState.messages.map((message, key) => {
                                    return <InboxMessage
                                        key={key}
                                        sender={message[inboxCons.messages.SENDER]}
                                        offerType={message[inboxCons.messages.SUBJECT]}
                                        projectName={message[inboxCons.messages.ORDER_NAME]}
                                        acceptRequest={() => this.props.acceptRequest(
                                            message[inboxCons.messages.SENDER],
                                            message[inboxCons.messages.SLUG]
                                        )}
                                        declineRequest={() => this.props.declineRequest(
                                            message[inboxCons.messages.SENDER],
                                            message[inboxCons.messages.SLUG]
                                        )}
                                    />
                                }) :
                                <NoMessage/>
                            }
                            </tbody>
                        </Table>



                        : <LoadingScreen/>}
                </Col>
            </Row>
        );
    }
}

function mapStateToProps(state) {
    return {
        customerInboxState: state.customerInboxReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCustomerInbox,
        acceptRequest,
        declineRequest,
        clearCustomerInbox
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(InboxMessages);