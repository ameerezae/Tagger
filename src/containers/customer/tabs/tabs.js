import React from 'react';
import {Col, Dropdown, Icon, Row} from "react-materialize";
import {NavLink} from "react-router-dom";

const Tabs = () => {
    return (
        <div className="hide-on-small-only">
            <div className="">
                <nav className="white vazir-font-medium">
                    <div className=" nav-wrapper">
                        <ul className="right">
                            <li>
                                <NavLink to="/customer/inbox">
                                    <Row>
                                        <Col><Icon className="grey-text">message</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>پیام ها</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/customer/my-orders">
                                    <Row>
                                        <Col><Icon className="grey-text">publish</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>سفارشات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/customer/create-order">
                                    <Row>
                                        <Col><Icon className="grey-text">mode_edit</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>ایجاد سفارش</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li className="center">
                                <NavLink to={"/customer/info"}>
                                    <Row>
                                        <Col className="right-align">
                                            <Icon tiny={true} className="grey-text tab-icons inline">timeline </Icon>
                                        </Col>
                                        <Col className="grey-text left-align">
                                            <span>اطلاعات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    );
};

export default Tabs;