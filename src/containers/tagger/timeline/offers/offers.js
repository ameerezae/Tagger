import React, {Component} from 'react';
import {Row, Col, Preloader} from "react-materialize";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getCustomerOrders, clearReducer, getCustomerOrdersFromPage} from "../../../../actions/tagger/taggerActions";
import * as taggerCons from "../../../../constants/taggerConstants";
import Offer from "./offer";
import BottomScrollListener from 'react-bottom-scroll-listener';
import * as apiUrls from "../../../../api/tagger/apiUrls";
import url_helper from "../../../../api/url_helper";
import LoadingScreen from "../../../../components/loading-screen/loading-screen";

class Offers extends Component {
    componentWillMount() {
        this.props.getCustomerOrders();
    }

    componentWillUnmount() {
        this.props.clearReducer();
    }

    render() {
        return (
            <div className="mr50 ml50 mt40 ">
                {this.props.timelineState.isOrdersFetched ?
                    <Row>
                        <Row>
                            <h6 className="grey-text vazir-font-medium right ">پیشنهادات</h6>
                        </Row>
                        {this.props.timelineState.orders[taggerCons.ORDERS.RESULTS].map((order, key) => {
                            return (
                                <Offer
                                    to={url_helper.removeServerAddress(order[taggerCons.ORDERS.ORDER_URL], "offers/")}
                                    subject={order[taggerCons.ORDERS.SUBJECT]}
                                    content={order[taggerCons.ORDERS.CONTENT]}
                                    type={order[taggerCons.ORDERS.TYPE]}
                                    taggerNo={order[taggerCons.ORDERS.TAGGER_NO]}
                                    price={order[taggerCons.ORDERS.PRICE]}
                                    creationDate={order[taggerCons.ORDERS.CREATION]}
                                    expirationDate={order[taggerCons.ORDERS.EXPIRED]}

                                />
                            )
                        })}
                        <Row className="center">
                            {this.props.timelineState.fetchingFromPage ? <Preloader
                                active
                                color="blue"
                                flashing
                            /> : null}
                        </Row>
                        <BottomScrollListener offset={200} onBottom={() => {
                            this.props.timelineState.orders[taggerCons.ORDERS.NEXT_URL] ? this.props.getCustomerOrdersFromPage(this.props.timelineState.orders[taggerCons.ORDERS.NEXT_URL]) : console.log("nothing")
                        }}/>
                    </Row>
                    : <LoadingScreen/>
                }

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        timelineState: state.timelineReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCustomerOrders,
        clearReducer,
        getCustomerOrdersFromPage
    }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(Offers);