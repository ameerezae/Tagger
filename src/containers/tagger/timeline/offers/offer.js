import React, {Component} from 'react';
import {Col, Icon, Row} from "react-materialize";
import BlueTick from "../../../../assets/png/Blue-tick-png.png";
import {Link} from "react-router-dom";
import NumberFormat from "react-number-format";

class Offer extends Component {
    render() {
        return (
            <Link to={this.props.to} target="_blank">
                <Row className="directionRightToLeft">
                    <Col push="m3 l2" s={12} m={9} l={8}>
                        <Row>
                            <h5 className="vazir-font-medium black-grey-text">{this.props.subject}</h5>
                        </Row>
                        <Row>
                            <p className="vazir-font-medium black-grey-text">{this.props.content.slice(0, 100)}.....</p>
                        </Row>
                        <Row className="directionRightToLeft iran-sans-font order-details">
                            <Col s={6} m={4} l={4}><p>
                                <Row className="valign-wrapper">
                                    <Icon className="ml10">info</Icon>
                                    نوع :
                                    &nbsp;&nbsp;
                                    {this.props.type}
                                </Row>
                            </p></Col>
                            <Col s={6} m={4} l={4}>
                                <p>
                                    <Row className="valign-wrapper Btitr-font">
                                        <Icon className="ml10">person</Icon>
                                        <div className="iran-sans-font">تعداد تگرها :</div>
                                        &nbsp;&nbsp;
                                        {this.props.taggerNo}
                                    </Row>
                                </p>
                            </Col>
                            <Col s={12} m={4} l={4} >
                                <p>
                                    <Row className="valign-wrapper Btitr-font">
                                        <Icon className="ml10">credit_card</Icon>
                                        <div className="iran-sans-font">بودجه :</div>
                                        &nbsp;&nbsp;
                                        <NumberFormat value={this.props.price} displayType={'text'} thousandSeparator={true} />
                                        &nbsp;&nbsp;
                                        <div className="iran-sans-font">ریال</div>
                                    </Row>
                                </p>
                            </Col>
                        </Row>

                    </Col>
                    <Col pull="m9 l8" s={12} m={3} l={2} className="vazir-font-medium mt20 mb10"
                         style={{color: "#32325d "}}>
                        <Row>
                            <Col s={6} m={12} l={12} className="valign-wrapper Btitr-font mt20">
                                <Icon className="ml10">date_range</Icon>
                                <div className="vazir-font-medium">ایجاد :</div>
                                &nbsp;&nbsp;
                                {this.props.creationDate.replace("-", "/").replace("-", "/")}
                            </Col>
                            <Col s={6} m={12} l={12} className="valign-wrapper Btitr-font mt20">
                                <Icon className="ml10">done_all</Icon>
                                <div className="vazir-font-medium">پایان :</div>
                                &nbsp;&nbsp;
                                {this.props.expirationDate.replace("-", "/").replace("-", "/")}
                            </Col>
                        </Row>
                    </Col>
                    <Col l={2} className="center hide-on-med-and-down">
                        <Row>
                            <img src={BlueTick} alt=""
                                 style={{width: "80px"}} className="center mr0 mt20 ml0 responsive-img"/>
                        </Row>
                    </Col>
                </Row>
                <hr className="grey-text grey"/>
            </Link>
        );
    }
}

export default Offer;