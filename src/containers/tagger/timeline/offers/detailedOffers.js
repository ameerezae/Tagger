import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getSingleCustomerOrderDetail, clearSingleOrderDetail} from "../../../../actions/tagger/taggerActions";
import DetailedOffer from "./detailedOffer";
import url_helper from "../../../../api/url_helper";
import {Col} from "react-materialize"
import * as taggerCons from "../../../../constants/taggerConstants";
import {sendCoOperateRequest} from "../../../../actions/tagger/order-accept-actions";

class DetailedOffers extends Component {
    componentWillMount() {
        this.props.getSingleCustomerOrderDetail(url_helper.addServerAddress(this.props.match.params.owner,this.props.match.params.slug));

    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.timelineState.isOrderDetailFetched) this.setTitleOfPage(this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.SUBJECT])
    }

    componentWillUnmount() {
        this.props.clearSingleOrderDetail();
    }

    setTitleOfPage = (title) => {
        if(title){
            return document.title = title;
        }
    };
    render() {
        return (
            <Col className="mr50 ml50 mt40 ">
                {this.props.timelineState.isOrderDetailFetched ?

                    <DetailedOffer
                        subject={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.SUBJECT]}
                        price={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.PRICE]}
                        tagger_no={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.TAGGER_NO]}
                        type={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.TYPE]}
                        content={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.CONTENT]}
                        creationDate = {this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.CREATION_DATE]}
                        expireDate={this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.EXP_DATE]}
                        sendCoOperate={()=>this.props.sendCoOperateRequest(
                            this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.OWNER],
                            this.props.timelineState.orderDetail[taggerCons.SINGLE_ORDER.SLUG]
                        )}
                    />

                    :null}

            </Col>

        );
    }
}
function mapStateToProps(state) {
    return {
        timelineState: state.timelineReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getSingleCustomerOrderDetail,
        clearSingleOrderDetail,
        sendCoOperateRequest
    }, dispatch)

}

export default connect(mapStateToProps,mapDispatchToProps)(DetailedOffers);