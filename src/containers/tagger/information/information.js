import React, {Component} from 'react';
import {CardPanel, Col, Row, Icon, Collection,CollectionItem, Pagination} from "react-materialize";
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize.min';
import "../scss/tagger.scss";

class Information extends Component {
    state = {

        items : [1,2,3,4,5,6,7,8,9,10,11,12,13],
        currentPage : 1,
        itemsPerPage : 4,
    }
    render() {

        const {items, currentPage, itemsPerPage} = this.state;
        const indexOfLast = currentPage * itemsPerPage;
        const indexOfFirst = indexOfLast - itemsPerPage;
        const currentItems = items.slice(indexOfFirst,indexOfLast);
        return (
            <Col className="mr30 ml30 mt60">
                <Row>
                    <Row className="directionRightToLeft mb20">
                        <h5 className="grey-text vazir-font-medium"> اطلاعات اجمالی</h5>
                    </Row>
                    <Col s={12} m={6} l={3}>
                        <CardPanel className="card-no-1 hoverable">
                            <Row>
                                <Col s={4}>
                                    <Icon className="white-text large">
                                        filter_7
                                    </Icon>
                                </Col>
                                <Col s={8} className="right-align vazir-font-medium">
                                    <h4 className="white-text Btitr-font">180</h4>
                                    <h6 className="white-text">امتیاز</h6>
                                </Col>
                            </Row>
                            <hr/>
                        </CardPanel>
                    </Col>
                    <Col s={12} m={6} l={3}>
                        <CardPanel className="card-no-2 hoverable">
                            <Row>
                                <Col s={4}>
                                    <Icon className="white-text large">
                                        credit_card
                                    </Icon>
                                </Col>
                                <Col s={8} className="right-align vazir-font-medium">
                                    <h4 className="white-text Btitr-font">2500+</h4>
                                    <h6 className="white-text">مبلغ دریافتی</h6>
                                </Col>
                            </Row>
                            <hr/>
                        </CardPanel>
                    </Col>
                    <Col s={12} m={6} l={3}>
                        <CardPanel className="card-no-3 hoverable">
                            <Row>
                                <Col s={4}>
                                    <Icon className="white-text large">
                                        visibility
                                    </Icon>
                                </Col>
                                <Col s={8} className="right-align vazir-font-medium">
                                    <h4 className="white-text Btitr-font">80.45</h4>
                                    <h6 className="white-text">میانگین دقت</h6>
                                </Col>
                            </Row>
                            <hr/>
                        </CardPanel>
                    </Col>
                    <Col s={12} m={6} l={3}>
                        <CardPanel className="card-no-4 hoverable">
                            <Row>
                                <Col s={4}>
                                    <Icon className="white-text large">
                                        done_all
                                    </Icon>
                                </Col>
                                <Col s={8} className="right-align vazir-font-medium">
                                    <h4 className="white-text Btitr-font">16.00</h4>
                                    <h6 className="white-text">تعداد پروژه های انجام شده</h6>
                                </Col>
                            </Row>
                            <hr/>
                        </CardPanel>
                    </Col>
                </Row>
                <Row className="mt30">
                    <Col s={12} m={12} l={6} offset="l6" className="directionRightToLeft">
                        <Row className="directionRightToLeft mb20">
                            <h5 className="grey-text vazir-font-medium">پروژه های انجام شده</h5>
                        </Row>
                        <Collection>
                            {currentItems.map((element)=>{
                                return(
                                    <CollectionItem className="avatar">
                                        <span className="step circle Btitr-font">{element}</span>
                                        <span className="title">
                                            <Row>
                                                <h6 className="vazir-font-medium black-text">ایجاد crm جهت مدیریت بخش های مختلف</h6>
                                            </Row>
                                        </span>
                                        <p>
                                            <Row className="valign-wrapper Btitr-font">
                                                <Icon className="ml10">done_all</Icon>
                                                10  روزه
                                            </Row>
                                            <br />
                                            <Row className="directionRightToLeft vazir-font-medium"
                                                 style={{color: "#32325d"}}>
                                                <Col s={3} m={3} l={2} ><p>
                                                    <Row className="valign-wrapper">
                                                        <Icon className="ml10">info</Icon>
                                                        متنی
                                                    </Row>
                                                </p></Col>
                                                <Col s={3} m={3} l={2}><p>
                                                    <Row className="valign-wrapper">
                                                        <Icon className="ml10">person</Icon>
                                                        شماره یک
                                                    </Row>
                                                </p></Col>
                                                <Col s={3} m={3} l={2}><p>
                                                    <Row className="valign-wrapper Btitr-font">
                                                        <Icon className="ml10">credit_card</Icon>
                                                        15000
                                                    </Row>
                                                </p></Col>
                                            </Row>
                                        </p>
                                        <a
                                            className="secondary-content"
                                            href="javascript:void(0)"
                                        >

                                        </a>
                                    </CollectionItem>
                                )
                            })
                            }
                        </Collection>
                        <Row className="center">
                            <Pagination
                                className="Btitr-font"
                                activePage={1}
                                items={Math.floor(this.state.items.length / this.state.itemsPerPage)+1}
                                leftBtn={<Icon>chevron_right</Icon>}
                                maxButtons={1}
                                rightBtn={<Icon>chevron_left</Icon>}
                                onSelect = {(page) => {this.setState({currentPage : page})} }
                            />
                        </Row>

                    </Col>
                </Row>
            < /Col>
         );
     }
}
export default Information;