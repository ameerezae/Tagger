import React, {Component} from 'react';
import {Row, Col, Collection} from "react-materialize";
import {connect} from "react-redux";
import * as inboxCons from "../../../../constants/tagger/tagger-inbox-constants";
import Message from "./message";
import {bindActionCreators} from "redux";
import {getTaggerInbox, clearTaggerInbox} from "../../../../actions/tagger/order-accept-actions";
import LoadingScreen from "../../../../components/loading-screen/loading-screen";
import NoMessage from "../no-message";

class Messages extends Component {
    componentWillMount() {
        this.props.getTaggerInbox();
    }

    componentWillUnmount() {
        this.props.clearTaggerInbox();
    }

    render() {
        return (
            <Row>
                <Col
                    s={12}
                >
                    {this.props.taggerInboxState.isMessageFetched ?
                        this.props.taggerInboxState.messages.length !== 0 ?
                            <div style={{border: "none"}}>
                                {this.props.taggerInboxState.messages.map((message, key) => {
                                    return <Message
                                        key={key}
                                        subject={message[inboxCons.TAGGER_INBOX.SUBJECT]}
                                        content={message[inboxCons.TAGGER_INBOX.CONTENT]}
                                        sender={message[inboxCons.TAGGER_INBOX.SENDER]}
                                    />
                                })}
                            </div>
                            : <NoMessage/>
                        : <LoadingScreen/>}

                </Col>
            </Row>
        );
    }
}

function mapStateToProps(state) {
    return {
        taggerInboxState: state.taggerInboxReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTaggerInbox,
        clearTaggerInbox,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Messages);