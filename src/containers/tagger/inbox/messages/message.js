import React from 'react';
import {CollectionItem, Icon, Row, Col} from "react-materialize";
import "../inbox.scss";
import * as inboxCons from "../../../../constants/tagger/tagger-inbox-constants";


const Message = (props) => {
    return (
        <div className="directionRightToLeft iran-sans-font ">
            <div>
                <a>
                    <Icon>
                        message
                    </Icon>
                </a>
                <span className="mr10">
                    <Row>
                        <Col s={8} m={4} l={4} offset="l6 m4" className="valign-wrapper">
                            <Icon>person_outline</Icon>
                            <span>فرستنده: &nbsp;</span>
                            <div className="message-sender">{props.sender}</div>
                        </Col>
                        <Col s={4} m={4} l={2}
                             className={props.subject === inboxCons.INBOX_SUBJECTS.ACCEPT ? "valign-wrapper accept-message" :
                                 props.subject === inboxCons.INBOX_SUBJECTS.DECLINE ? "valign-wrapper decline-message" : "valign-wrapper"}>
                            {props.subject}
                        </Col>
                    </Row>
                </span>
                <p className="grey-text-custom message-line-height mt10">
                    {props.content}
                </p>
                <hr/>
            </div>
        </div>
    );
};

export default Message;