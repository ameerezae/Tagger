import React, {Component} from 'react';
import Messages from "./messages/messages";
import {Row} from "react-materialize";
import "./inbox.scss";
class Inbox extends Component {
    render() {
        return (
            <div className=" mt30 mr20 ml20">
                <Row className="directionRightToLeft valign-wrapper">
                    <h6 className="iran-sans-font mr20 grey-text-custom">
                        پیام ها
                    </h6>
                </Row>
                <div className="margin-x-inbox mt20 ">
                    <Messages/>
                </div>
            </div>
        );
    }
}

export default Inbox;