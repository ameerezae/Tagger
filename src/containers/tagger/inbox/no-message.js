import React from 'react';
import {Col} from "react-materialize";

const NoMessage = () => {
    return (
        <Col s={12} l={10} offset="l1" className="center mt30">
            <div className="no-order">
                <img src="https://img.icons8.com/cotton/64/000000/mailing.png" alt="mail"/>
                <div className="iran-sans-font grey-text-custom">پیام  جدیدی موجود نیست</div>
            </div>
        </Col>
    );
};

export default NoMessage;