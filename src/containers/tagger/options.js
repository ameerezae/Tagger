import React from 'react';
import Offers from "./timeline/offers/offers";
import Information from "./information/information";
import {Route} from "react-router-dom";
import DetailedOffers from "./timeline/offers/detailedOffers";
import Inbox from "./inbox/inbox";

const Options = () => {
    return (
        <div>
            <Route exact path="/tagger/offers" component={Offers}/>
            <Route exact path="/tagger/info" component={Information}/>
            <Route exact path="/tagger/offers/:owner/:slug/" component={DetailedOffers}/>
            <Route exact path="/tagger/inbox" component={Inbox}/>
        </div>
    );
};

export default Options;