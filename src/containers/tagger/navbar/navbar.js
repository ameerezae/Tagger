import React, {Component} from 'react';
import {Dropdown, Button, Divider, Icon, Row, Col} from "react-materialize";
import LOGO from "../../../assets/png/32-336319_tag-icon-svg-hd-png-download.png";
import {NavLink} from "react-router-dom";
import Cookies from "js-cookie";
import * as universalConstants from "../../../constants/constants";
import {connect} from "react-redux";
import * as profileCons from "../../../constants/tagger/tagger-profile-constants";
import * as apiUrls from "../../../api/main_api_urls";


class TaggerNavbar extends Component {
    render() {
        return (
            <div className="navbar-fixed">
                <nav className="tagger-navbar-color vazir-font-medium">
                    <div className=" nav-wrapper">
                        <ul id="nav-mobile" className="left hide-on-small-only">
                            <li>
                                <a href={apiUrls.SSO_URL} onClick={()=>{Cookies.remove(universalConstants.ACCESS_TOKEN ,{domain: apiUrls.ARMANKADEH_DOMAIN})}}>
                                    <Row className="vertical_align">
                                        <Col s={6} className="">
                                            {' '}خروج
                                        </Col>
                                        <Col s={6}>
                                            <Icon className="">
                                                exit_to_app
                                            </Icon>
                                        </Col>
                                    </Row>
                                </a>
                            </li>
                        </ul>
                        <ul className="right">
                            <li><a href="#"><Icon className="">message</Icon></a></li>
                            {this.props.taggerProfileState.isProfileFetched ?
                                <li className="">
                                    <a href="/tagger/info">
                                        {`@ ${this.props.taggerProfileState.profile[profileCons.TAGGER_PROFILE.USER_NAME]}`}
                                    </a>
                                </li>
                                : null
                            }
                            <li><img src={LOGO} className="navbar-logo" alt="logo"/></li>
                        </ul>
                        <ul className="hide-on-med-and-up show-on-small vazir-font-medium">
                            <Dropdown
                                options={{
                                    alignment: 'left',
                                    autoTrigger: true,
                                    closeOnClick: true,
                                    constrainWidth: true,
                                    container: null,
                                    coverTrigger: true,
                                    hover: false,
                                    inDuration: 150,
                                    outDuration: 250
                                }}
                                trigger={<li><a href="sass.html"><Icon>menu</Icon></a></li>}
                                className="vazir-font-medium"
                            >
                                <NavLink to={"/tagger/info"}>
                                    <Row>
                                        <Col><Icon medium className="small">timeline</Icon></Col>
                                        <Col className="left-align vazir-font-medium">
                                            <span>اطلاعات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                                <NavLink to="/tagger">
                                    <Row>
                                        <Col><Icon medium className="small">schedule</Icon></Col>
                                        <Col className="left-align vazir-font-medium">
                                            <span>درحال انجام</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                                <NavLink to="/tagger/offers">
                                    <Row>
                                        <Col><Icon className="small">play_for_work</Icon></Col>
                                        <Col className="left-align vazir-font-medium">
                                            <span>پیشنهادات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                                <NavLink to="/tagger/inbox">
                                    <Row>
                                        <Col><Icon className="small">message</Icon></Col>
                                        <Col className="left-align vazir-font-medium">
                                            <span>پیام ها</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                                <a href="/authentication" onClick={() => {
                                    Cookies.remove(universalConstants.ACCESS_TOKEN)
                                }}>
                                    <Icon>
                                        exit_to_app
                                    </Icon>
                                    {' '}
                                    خروج
                                </a>
                            </Dropdown>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        taggerProfileState : state.taggerProfileReducer,
    }
}

export default connect(mapStateToProps)(TaggerNavbar);
