import React from 'react';
import {Col, Dropdown, Icon, Row} from "react-materialize";
import {NavLink} from "react-router-dom";

const Tabs = () => {
    return (
        <div className="hide-on-small-only">
            <div className="navbar-fixed">
                <nav className="white vazir-font-medium">
                    <div className=" nav-wrapper">
                        <ul className="right">
                            <li>
                                <NavLink to="/tagger/inbox">
                                    <Row>
                                        <Col><Icon className="grey-text small">message</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>پیام ها</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/tagger/offers">
                                    <Row>
                                        <Col><Icon className="grey-text small">play_for_work</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>پیشنهادات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/tagger">
                                    <Row>
                                        <Col><Icon medium className="grey-text small">schedule</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>درحال انجام</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                            <li className="center">
                                <NavLink to={"/tagger/info"}>
                                    <Row>
                                        <Col><Icon medium className="grey-text small">timeline</Icon></Col>
                                        <Col className="grey-text left-align">
                                            <span>اطلاعات</span>
                                        </Col>
                                    </Row>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    );
};

export default Tabs;