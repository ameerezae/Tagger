import React, {Component} from 'react';
import "./scss/tagger.scss";
import "../../assets/helper.css";
import {Col, Row, ProgressBar} from "react-materialize";
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize.min';
import TaggerNavbar from "./navbar/navbar";
import Tabs from "./tabs/tabs";
import {connect} from "react-redux";
import Options from "./options";
import {getTaggerProfile} from "../../actions/tagger/profile-actions";
import {bindActionCreators} from "redux";
import Cookies from "js-cookie";
import * as universalConstants from "../../constants/constants";
import M from "materialize-css";



class Tagger extends Component {
    componentWillMount() {
        if(!this.isAuthorized()) {
            M.toast({html: 'ابتدا وارد شوید', classes : "white-text red vazir-font-medium"});
        }else{
            this.props.getTaggerProfile();
        }
    }

    isAuthorized = () => {
        const token = Cookies.get(universalConstants.ACCESS_TOKEN);
        return !!token;
    };

    render() {
        return (
            <Col>
                <Col >
                    <TaggerNavbar/>
                    <Tabs/>
                    <Options/>
                </Col>
            </Col>

        );
    }
}


function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getTaggerProfile,
    },dispatch)
}

export default connect(null, mapDispatchToProps)(Tagger);