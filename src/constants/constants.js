import Cookies from "js-cookie";

export const ACCESS_TOKEN = "access-token";

export const TOKEN = "token";

export const HEADERS_WITHOUT_AUTH = {
    "Content-type": "application/json"
}
export const CONFIG_WITH_AUTH = (accessToken) => ({
    headers:
        {
            Authorization: `JWT ${accessToken}`,
        },
});

export const CONFIG_WITH_AUTH_AND_QUERY = (accessToken, query) => ({
    headers :{
        Authorization: `JWT ${accessToken}`,
    },
    params : query,
});






export const REQUESTS_STATUS_NUMBERS = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    FORBIDDEN : 403,
};