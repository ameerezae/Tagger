export const ORDERS = {
    COUNT : "count",
    NEXT_URL : "next",
    PREVIOUS : "previous",
    RESULTS : "results",
    SUBJECT : "subject",
    CONTENT : "content",
    TAGGER_NO : "tagger_number",
    TYPE : "type",
    ORDER_URL : "order_url",
    PRIVATE : "is_private",
    EXPIRED : "expiration_date",
    PRICE: "price",
    CREATION: "creation_date",
};

export const SINGLE_ORDER = {
    SUBJECT : "subject",
    CONTENT: "content",
    TAGGER_NO : "tagger_number",
    TYPE : "type",
    SLUG : "slug",
    CREATION_DATE : "creation_date",
    PRIVATE : "is_private",
    PRICE: "price",
    EXP_DATE: "expiration_date",
    OWNER : "owner"
};