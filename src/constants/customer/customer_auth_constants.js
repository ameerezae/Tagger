export const LOGIN_ERRORS = {
    NON_FIELD : "non_field_errors",
    EMAIL : "email",
    PASSWORD : "password",
};


export const REGISTER_ERRORS = {
    EMAIL : "email",
    USERNAME : "username",
    PHONE : "phone_number",
    PASS1 : "password1",
    PASS2 : "password2",
    NON_FIELD : "non_field_errors"
}