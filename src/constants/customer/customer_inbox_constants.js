export const messages = {
    SENDER : "sender",
    RECEIVER : "receiver",
    SUBJECT : "subject",
    CONTENT : "content",
    ORDER_NAME : "order",
    ORDER_URL : "order_url",
    SLUG : "order_slug",
};