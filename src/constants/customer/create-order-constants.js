export const NEGATIVE_NUMBER_ERROR = "اعداد منفی مورد پذیرش نیستند";

export const CO_ERRORS = {
    SUBJECT: "subject",
    CONTENT: "content",
    TYPE: "type",
    DURATION: "duration",
};


export const ORDERS = {
    SUBJECT: "subject",
    CONTENT : "content",
    TAGGER : "tagger_number",
    TYPE : "type",
    PRIVATE : "isPrivate",
    PRICE : "price",
    DURATION: "duration",
};
