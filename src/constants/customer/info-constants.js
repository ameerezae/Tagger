export const PROFILE = {
    USERNAME : "username",
    FIRST_NAME : "first_name",
    LAST_NAME : "last_name",
    EMAIL : "email",
    IMAGE : "image",
    PHONE_NUMBER : "phone_number",
    ADDRESS : "address",
};


export const ORDERS = {
    COUNT : "count",
    NEXT : "next",
    PREVIOUS : "previous",
    RESULTS : "results",
    SUBJECT : "subject",
    CONTENT : "content",
    TAGGER : "tagger_number",
    TYPE : "type",
    CREATION_TIME : "creation_datetime",
    CREATION_DATE : "creation_date",
    URL : "order_url",
    EXP_DATE : "expiration_date",
    PRICE : "price",
    IS_PRIVATE : "isPrivate",
    SLUG : "slug",
    OWNER : "owner",
};


export const SINGLE_ORDER = {
    SUBJECT : "subject",
    CONTENT: "content",
    TAGGER_NO : "tagger_number",
    TYPE : "type",
    SLUG : "slug",
    CREATION_DATE : "creation_date",
    PRIVATE : "is_private",
    PRICE: "price",
    EXP_DATE: "expiration_date",
};

