export const TAGGER_INBOX = {
    SENDER : "sender",
    RECEIVER : "receiver",
    SUBJECT : "subject",
    CONTENT : "content",
    ORDER_NAME : "order",
    ORDER_URL : "order_url",
    SLUG : "slug",
};

export const INBOX_SUBJECTS = {
    ACCEPT : "پذیرش درخواست",
    DECLINE : "عدم پذیرش درخواست",
};