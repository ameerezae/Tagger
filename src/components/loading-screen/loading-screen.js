import React from 'react';
import "./loading-screen.scss";
const LoadingScreen = () => {
    return (
        <div className="wrapper">

            <div id="loader-wrapper">

                <div id="loader">
                    <p className="grey-text vazir-font-medium">منتظر بمانید</p>
                    <div className="circ-one"></div>
                    <div className="circ-two"></div>
                </div>

                <div className="loader-section section-left"></div>
                <div className="loader-section section-right"></div>

            </div>

        </div>
    );
};

export default LoadingScreen;